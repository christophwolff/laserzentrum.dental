<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package laserzentrum.dental
 */

get_header(); ?>


<?php
get_sidebar();
get_footer();
