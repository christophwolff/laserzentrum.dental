<?php
/**
 * laserzentrum.dental functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package laserzentrum.dental
 */

/**
 * Enqueue scripts and styles.
 */
function laserzentrum_dental_scripts() {
	

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), wp_get_theme()->get('Version') );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', array(), wp_get_theme()->get('Version') );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.css', array(), wp_get_theme()->get('Version') );

	wp_enqueue_style( 'laserzentrum-dental-style', get_stylesheet_uri() );



	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), wp_get_theme()->get('Version'), true );
	wp_enqueue_script( 'jquery-smooth-scroll', get_template_directory_uri() . '/js/jquery.smooth-scroll.min.js', array('jquery'), wp_get_theme()->get('Version'), true );
	wp_enqueue_script( 'laserzentrum-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), wp_get_theme()->get('Version'), true );
	wp_enqueue_script( 'viewport', get_template_directory_uri() . '/js/viewport.js', array('jquery'), wp_get_theme()->get('Version'), true );

	if(is_page("Team")){

		wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.js', array('jquery'), wp_get_theme()->get('Version'), true );
		wp_enqueue_script( 'team-slider', get_template_directory_uri() . '/js/team-slider.js', array('jquery'), wp_get_theme()->get('Version'), true );
		wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/css/owl-carousel/owl.carousel.css', array(), wp_get_theme()->get('Version'), true );
		wp_enqueue_style( 'owl-carousel-theme-css', get_template_directory_uri() . '/css/owl-carousel/owl.theme.css', array(), wp_get_theme()->get('Version'), true );


	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'laserzentrum_dental_scripts' );


function laserzentrum_navigation_menus() {
	register_nav_menus( array(
		'main_header_menu'		=> 'Main Header Navigation',
		'footer_menu_1' 			=> 'First Footer Menu',
		'footer_menu_2' 			=> 'Secound Footer Menu'
	) );
}

add_action( 'init', 'laserzentrum_navigation_menus' );


/**
 * Check and alert if CF7 Plugin is not active
 */
if( !function_exists( 'wpcf7' ) ) {
  add_action( 'admin_notices', 'wpcf7_notice' );
}
function wpcf7_notice() {
  ?>
  <div class="error">
      <p><?php _e( '<strong>Wichtig!</strong><br><br>Bitte installieren und aktivieren Sie das Plugin "Contact Form 7"<br>Dieses Theme benötigt das Plugin.', 'laserzentrum_textdomain' ); ?></p>
  </div>
  <?php
}

/**
 * Check and alert if ACF Plugin is not active
 */
if( !function_exists( 'the_field' ) ) {
  add_action( 'admin_notices', 'acf_notice' );
}
function acf_notice() {
  ?>
  <div class="error">
      <p><?php _e( '<strong>Wichtig!</strong><br><br>Bitte installieren oder aktivieren Sie das Plugin "ACF - Advanced Custom Fields" in Version 5.0 bzw. Pro<br>Dieses Theme benötigt das Plugin.', 'br-bayern1_textdomain' ); ?></p>
  </div>
  <?php
}

/**
 * Remove ACF Menu for Distribution
 *
 **/

//add_filter('acf/settings/show_admin', '__return_false');

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}


// Custom Excerpts
function laserzentrum_excerpt_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 50;
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Create the Custom Excerpts callback
function laserzentrum_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}


// Remove 'text/css' from our enqueued stylesheet
function style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}


add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

add_theme_support('post-thumbnails');
add_image_size( 'news', 1882, 1060, true ); // (cropped)
add_filter('jpeg_quality', function($arg){return 40;});

// Register Custom Navigation Walker
//require_once('inc/laserzentrum_nav_walker.php');


function team_cpt() {

  $labels01 = array(
      'name' => _x('Team', 'post type general name'),
      'singular_name' => _x('Mitarbeiter/in', 'post type singular name'),
      'add_new' => _x('Neuen Mitarbeiter/in', 'Autor'),
      'add_new_item' => __('Neuen Mitarbeiter/in hinzufügen'),
      'edit_item' => __('Mitarbeiter/in editieren'),
      'new_item' => __('Neuen Mitarbeiter/in'),
      'all_items' => __('Alle Mitarbeiter/in'),
      'view_item' => __('Mitarbeiter/in ansehen'),
      'search_items' => __('Mitarbeiter/in suchen'),
      'not_found' =>  __('Keine Mitarbeiter/in gefunden'),
      'not_found_in_trash' => __('Kein Mitarbeiter/in im Trash gefunden'),
      'parent_item_colon' => '',
      'menu_name' => __('Team')

    );
    $args01 = array(
      'labels' => $labels01,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'team'),
      'capability_type' => 'post',
      'has_archive' => true,
      'hierarchical' => false,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-tag',
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt','tag' ),
    );
    register_post_type('team',$args01);

}

add_action( 'init', 'team_cpt' );

