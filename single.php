<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package laserzentrum.dental
 */

get_header(); ?>


<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<section class="container-fluid news-all">
	<article class="big" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	            <div class="row news-head">
	                <header class="col-sm-9">
	                	
	                	<h2>
							<?php the_title(); ?>
						</h2>

	                </header>
	                <div class="col-sm-3 text-right news-date"><?php the_time('F j, Y'); ?></div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="row">
	                <div class="col-md-12 news-img">
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail( 'news', array( 'class' => 'img-responsive' ) ); // Declare pixel size you need inside the array ?>
							</a>
						<?php endif; ?>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-9 news-content">
	                    <p class=""><?php the_content(); ?></p>
	                </div>
	            </div>
	        </article>

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, keine Artikel gefunden', 'laserzemtrum_textdomain' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>


</section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>
<?php
get_footer();
