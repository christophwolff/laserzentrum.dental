<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package laserzentrum.dental
 */

get_header(); ?>
<section class="container-fluid news-all">
<h1>Seite nicht gefunden</h1>

<p>Gehe zurück zur <a href="<?php echo get_bloginfo('blog_url'); ?>">Startseite.</a></p>
</section>
<?php
get_footer();
