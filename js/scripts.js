jQuery(document).ready(function(){


    //SMOOTH SCROLL
    jQuery('#scroll-element').smoothScroll({
        offset: 50,
    });

    // MAIN BANER IN HOME PAGE FULL height

    if (jQuery(window).width() < 768) {
        jQuery(".huge-top:not(.laserbaner)").css("height", jQuery(window).height() - 30);
    }

    // LASER - DASH UNDER LASER FOR MOBILE

    jQuery("#dash-long").height(jQuery("#dash-height").height());


    //FULL HEIGHT
    jQuery(".full-height-row").height(jQuery(".huge-top").height());
    jQuery(".bigquote-gradient").height(jQuery(".bigquote").height());

    // CENTER QUOTE TOP-BOTTOM
    if (jQuery(window).width() > 768) {
        jQuery(".quote").css("padding-top", ((jQuery(".huge-top").height() - jQuery(".quote").height())/2))
    }

    // VIDEO RATIO 24/13
    jQuery('.video').height(
        (jQuery('.video').width()*13)/24
    )



    // CENTER VIDEO button
    jQuery("#btn-vid").css("padding-top", ((jQuery(".video").height() - jQuery("#btn-vid").height())/2))

    // DARK COLLAPSE


    jQuery(".navbar-toggle").click(function(){

            jQuery(".navbar-default").toggleClass("collapse-bg");


    });

    // RETINA IMG SRC CHANGING
    // if (window.devicePixelRatio > 1) {
    //     var lowresImages = jQuery('img');
    //     lowresImages.each(function(i) {
    //         var lowres = jQuery(this).attr('src');
    //         var highres = lowres.replace(".", "@2x.");
    //         jQuery(this).attr('src', highres);
    //     });
    // }

    // MENU SLIDE

    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //

var szerokosc = jQuery(window).width();
      jQuery('.dropdown-pc').hover(
          function() {
               if( szerokosc > 991) {
                jQuery('.collapse .dropdown').find('.dropdown-menu')
                    .first()
                    .stop(true, true)
                    .slideDown(300);
                }
          },
          function() {
               if( szerokosc > 991) {
                jQuery('.collapse .dropdown').find('.dropdown-menu')
                    .first()
                    .stop(true, true)
                    .slideUp(300);
                }
          }
      );





});

jQuery(window).resize(function(){
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //

    // MAIN BANER IN HOME PAGE FULL height
    if (jQuery(window).height() < 752) {
        jQuery(".huge-top:not(.laserbaner)").height(
                jQuery(window).height() - 30
        );
    }
    else {
        jQuery(".huge-top:not(.laserbaner)").height(722);
    };

    //GRADIENT-TOP HEIGHT
    jQuery(".gradient-top").height(jQuery(".huge-top").height());
    jQuery(".full-height-row").height(jQuery(".huge-top").height());

    // CENTER QUOTE TOP-BOTTOM
    if (jQuery(window).width() > 768) {
        jQuery(".quote").css("padding-top", ((jQuery(".huge-top").height() - jQuery(".quote").height())/2));
    };

    // VIDEO RATIO 24/13
    jQuery('.video').height(
        (jQuery('.video').width()*13)/24
    );

    // LASER - DASH UNDER LASER FOR MOBILE

    jQuery("#dash-long").height(jQuery("#dash-height").height());


    // CENTER VIDEO button
    jQuery("#btn-vid").css("padding-top", ((jQuery(".video").height() - jQuery("#btn-vid").height())/2));


});
