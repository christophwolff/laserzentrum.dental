jQuery(document).ready(function ($) {
    jQuery("#slider ul li").width(
        jQuery(window).width()
    );
  jQuery('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });

	var slideCount = jQuery('#slider ul li').length;
	var slideWidth = jQuery(window).width();
	var slideHeight = jQuery('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;

	jQuery('#slider').css({ width: slideWidth});

	jQuery('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    jQuery('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        jQuery('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            jQuery('#slider ul li:last-child').prependTo('#slider ul');
            jQuery('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        jQuery('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            jQuery('#slider ul li:first-child').appendTo('#slider ul');
            jQuery('#slider ul').css('left', '');
        });
    };

    jQuery('button.control_prev').click(function () {
        moveLeft();
    });

    jQuery('button.control_next').click(function () {
        moveRight();
    });

});

jQuery(window).resize(function(){
    jQuery("#slider ul li").width(
        jQuery(window).width()
    );
    var slideCount = jQuery('#slider ul li').length;
	var slideWidth = jQuery(window).width();
	var slideHeight = jQuery('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;

	jQuery('#slider').css({ width: slideWidth});

	jQuery('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    jQuery('#slider ul li:last-child').prependTo('#slider ul');
});
