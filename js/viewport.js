jQuery(document).ready(function(){
    /**
     * inViewport jQuery plugin by Roko C.B. stackoverflow.com/questions/24768795/
     *
     * Returns a callback function with an argument holding
     * the current amount of px an element is visible in viewport
     * (The min returned value is 0 (element outside of viewport)
     * The max returned value is the element height + borders)
     */
    ;(function(jQuery, win) {
      jQuery.fn.inViewport = function(cb) {
         return this.each(function(i,el) {
           function visPx(){
             var elH = jQuery(el).outerHeight(),
                 H = jQuery(win).height(),
                 r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
             return cb.call(el, Math.max(0, t>0? Math.min(elH, H-t) : (b<H?b:H)));
           }
           visPx();
           jQuery(win).on("resize scroll", visPx);
         });
      };
    }(jQuery, window));

    jQuery("h2").inViewport(function(px) {
      //console.log( px ); // `px` represents the amount of visible height
      if(px){
          if (!jQuery(this).hasClass("animated")) {
              jQuery(this).addClass("animated fadeInUp");
          }
        // do this if element enters the viewport // px > 0
      }else{
        // do that if element exits  the viewport // px = 0
      }
    });

    jQuery(".animate-this").inViewport(function(px) {
      //console.log( px ); // `px` represents the amount of visible height
      if(px){
          if (!jQuery(this).hasClass("animated")) {
              jQuery(this).addClass("animated fadeInUp");
          }
        // do this if element enters the viewport // px > 0
      }else{
        // do that if element exits  the viewport // px = 0
      }
    });
});
