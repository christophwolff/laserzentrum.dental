<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laserzentrum.dental
 */

?>
<footer>
    <div class="container-fluid">

    <?php 

		    $footer_1 = array(
				'menu' 				=> 'footer_menu_1',
				'theme_location'	=> 'footer_menu_1',
				'container'       	=> 'div',
				'container_class' 	=> '',
				'fallback_cb'     	=> '',
				'echo'            	=> true,
				'items_wrap'      	=> '<ul class="nav nav-pills big-foot">%3$s</ul>',
				'depth'           	=> 0
				);

			wp_nav_menu ($footer_1);
		 ?>
		  <?php 

		    $footer_2 = array(
				'menu' 				=> 'footer_menu_2',
				'theme_location'	=> 'footer_menu_2',
				'container'       	=> 'div',
				'container_class' 	=> '',
				'fallback_cb'     	=> '',
				'echo'            	=> true,
				'items_wrap'      	=> '<ul class="nav nav-pills small-foot">%3$s</ul>',
				'depth'           	=> 0
				);

			wp_nav_menu ($footer_2);
		 ?>
    </div>
</footer>

<script type="text/javascript">
	
	jQuery(document).ready(function() {
            jQuery(".navbar-toggle").click(function() {
                if (jQuery(this).hasClass("collapsed")) {
                    jQuery(".navbar-brand").html('<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Laserzentrum Dental">');
                } else {
                    jQuery(".navbar-brand").html('<img src="<?php echo get_template_directory_uri(); ?>/img/logo-grey.png" alt="Laserzentrum Dental">');
                }
            });
        });

</script>
<?php wp_footer(); ?>

</body>
</html>
