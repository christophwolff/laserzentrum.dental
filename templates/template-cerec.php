<?php
/*
 * Template Name: CEREC
 *
 */

get_header(); ?>

 <section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-9">
            <h2>CEREC – die Keramikkrone <strong>ohne Abdruck</strong></h2>
        </header>
        <div class="col-sm-9">
        <p>Keramik hat Gold als Material für Kronen, Teilkronen oder Inlays vollkommen ersetzt. Nicht nur, weil Keramik der natürlichen Zahnsubstanz in Farbe, Transluzenz und Festigkeit wesentlich ähnlicher ist als Gold, sondern auch, weil Keramik sich am verbleibenden Zahn adhäsiv befestigen, quasi "festkleben", lässt. Dadurch entsteht eine echte Verbindung zwischen Keramik und Zahn. Dies erlaubt dem Zahnarzt, nur die geschädigten Anteile des Zahnes zu ersetzen und gesunde Substanz unversehrt zu lassen.
        <br/><br/>
        Durch die CEREC-Methode (CEramic REConstruction) ist es uns möglich, in einer einzigen Sitzung, ohne lästige Abdrücke und Korrekturen, Ihnen eine schöne, individuelle Keramikkrone anzufertigen und fest einzugliedern. </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 panel-separation">
            <h4>Wir arbeiten seit 1994 sehr erfolgreich mit Cerec. Unser aktuelles Cerec-Modell ist die puderlose Cerec-Omnicam.</h4>
        </div>
    </section>
    <div class="full-size-photo " id="f-s-p-5">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">
        <div class="col-sm-12 six-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Schont Ihre gesunde Zahnsubstanz</h3>
                <p>Eine CEREC Behandlung ist minimal invasiv. Das bedeutet für Sie, dass nur die kariösen Stellen oder die alte Amalgam- bzw. Kunststoffüllung des Zahnes entfernt wird. </p>
             </div>
            <div class="col-md-4">
                <h3>Lange Lebensdauer.</h3>
                <p>Vielfältige Studien zu CEREC beweisen, dass CEREC-Restaurationen die gleiche, langjährige Haltbarkeit haben wie früher Goldversorgungen.</p>
            </div>
            <div class="col-md-4">
                <h3>Perfekter Sitz </h3>
                <p>Die vollständig am Computer erstellte Restauration wird vollautomatisch aus einem Keramikblock passend zu Ihrem Zahn herausgeschliffen. Dies erfolgt so präzise, dass wir sie sofort passgenau und dauerhaft einsetzen können. </p>
            </div>
            <div class="clearfix"></div>
            <!-- BOTTOM PANEL -->
            <div class="col-md-4">
                <h3>Nur eine Behandlungssitzung</h3>
                <p>Wir brauchen kein lästiges Provisorium mehr, dass während des konventionellen Herstellungsprozesses eingesetzt wird. Auch das Abformmaterial ist nicht mehr notwendig. Die Krone/ Inlay wird in einer Sitzung fertiggestellt und eingesetzt. </p>
             </div>
            <div class="col-md-4">
                <h3>Perfektes Aussehen</h3>
                <p>CEREC-Keramiken sind zahnfarben, weisen eine natürliche Transluzenz auf, sind verfärbungssicher und können im Bedarfsfall sehr einfach an die Nachbarzähne angepasst werden. Ihre Fluoreszenz entspricht der von natürlichen Zähnen. Nach der Behandlung entdecken Sie keinen Unterschied zwischen dem Zahn und der Füllung. Daneben können mit CEREC auch ästhetische Korrekturen im Frontzahnbereich durchgeführt werden. </p>
            </div>
            <div class="col-md-4">
                <h3>Biokompatible Zahnkeramik </h3>
                <p>Die CEREC-Keramik ist ein Naturstoff (Feldspat-Keramik) und ist aufgrunddessen absolut verträglich. Sie verhält sich ähnlich wie gesunder Zahnschmelz reagiert nicht auf Kälte oder Hitze, und ist geschmacksneutral, da keine Wechselwirkungen mit anderen Zahnfüllungen im Mund auftreten.</p>
            </div>
        </div>
        <div class="col-sm-9 panel-separation">
            <h2>Ablauf</h2>
        </div>
        <div class="col-xs-10 circles-ul circles-bottom-padding">
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">1</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-long"></div>
                </div>
                <div class="col-sm-5 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Dreidimensionale Aufnahme vom zu behandelnden Zahn</p>
                        <p>Nach beschleifen des Zahnes in die passende Form nehmen wir mit der CEREC 3D Messkamera nehmen wir in nur wenigen Sekunden eine dreidimensionale Aufnahme vom zu behandelnden Zahn. Dabei wird auf einen konventionellen Abdruck, den viele Patienten als unangenehm empfinden, verzichtet. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">2</div>
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-sm-5 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Konstruktion der Krone</p>
                        <p>Am Bildschirm wird die Krone in Ihrem Beisein konstruiert.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">3</div>
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-sm-7 with-ul col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Erstellung der Krone</p>
                        <p>Danach schleift CEREC die Restauration vollautomatisch aus einem Keramikblock in wenigen Minuten heraus. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">4</div>
                </div>
                <div class="col-sm-7 with-ul col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Einpassen und Verkleben der Krone</p>
                        <p>Wir passen die Restauration sofort ein und verkleben sie sorgfältig mit dem Zahn. Nach der Behandlung können Sie sofort wieder essen und trinken. </p>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();