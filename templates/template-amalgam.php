<?php
/*
 * Template Name: Amalgam
 *
 */

get_header(); ?>

<section class="container-fluid leistungen">
    <header class=" col-xs-12 col-sm-9">
        <h2>Aus Amalgamfüllungen wird ständig, besonders beim Kauen, <strong>Quecksilber freigesetzt</strong></h2>
    </header>
    <div class="col-sm-10">
        <p>Amalgam gilt als das älteste Material für Zahnfüllungen und wird seit mehr als 150 Jahren in der zahnärztlichen Versorgung benutzt. Amalgam ist leicht herzustellen, preiswert und stellt keine besonderen Anforderungen an die Verarbeitung. Hauptdiskussionspunkt in der Öffentlichkeit ist der Quecksilberanteil bis zu 50% und die sich daraus ergebenden Gesundheitsrisiken.
        <br/><br/>
        Die Füllungen geben Quecksilberdämpfe ab, die zum grössten Teil in der Lunge aufgenommen werden. Kaugummikauen und nächtliches unbewusstes Zähneknirschen können die tägliche Aufnahme um ein Vielfaches erhöhen.</p>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 panel-separation konsenspapier">
        <h4>Das Konsenspapier des Bundesministeriums für Gesundheit, der Bundeszahnärztekammer, der KZBV und weiterer zahnärztlicher Gesellschaften enthält folgende <strong>Einschränkungen in der Amalgamanwendung</strong> (1997):</h4>
    </div>
    <div class="col-md-6 col-sm-9 col-xs-12 points-3">
        <p>1) Bei Schwangeren sollen möglichst keine Amalgamfüllungen gelegt bzw. entfernt werden.</p>
        <br/><br/>
        <p>2) Schwere Nierenfunktionsstörungen stellen eine relative Kontraindikation für die Anwendung von Amalgam dar. Es gibt hinreichende Publikationen, die die Niere als bevorzugtes Zielorgan für eine Quecksilbervergiftung beschreiben.</p>
        <br/><br/>
        <p>3) Da eine Behandlung mit Amalgam zu einer Belastung des Organismus mit Quecksilber führt, sollte aus Gründen des vorbeugenden Gesundheitsschutzes sorgfältig geprüft werden, ob eine Amalgamtherapie notwendig ist.</p>
    </div>
    <div class="col-xs-12 col-sm-9 panel-separation">
        <h2>Wir sind eine <strong>amalgamfreie Praxis</strong> aus Überzeugung! </h2>
    </div>
    <div class="col-sm-9">
        <h4>Mit jeder Amalgamfüllung ist auch eine höhere Quecksilberkonzentration im Blut und Urin nachweisbar! </h4>
    </div>
    <div class="col-sm-9 panel-separation">
        <p>Wir wollen jegliche Belastung (auch in geringer Konzentration)  des Körpers vermeiden und beraten Sie gerne zu alternativen Füllungsmaterielien.</p>
    </div>
</section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php
get_footer();