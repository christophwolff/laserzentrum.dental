<?php
/*
 * Template Name: Kontakt
 *
 */

get_header(); ?>
    <!-- NEWS -->
    <section class="container-fluid kontakt-all">
        <div class="row">
            <div class="col-md-5 col-xs-12 kontakt left-kontakt">
                <h4><strong>Urlaubszeit: Geänderte Sprechstunden</strong></h4>
                <div class="col-md-10 nopad">
                      <?php the_field('contact_notice'); ?>
                    <div class="left-kontakt-panels">
                        <strong>Mo. – Mi.</strong><br/><br/>
                        <strong>Do.</strong><br/><br/>
                        <strong>Fr.</strong>
                    </div>
                    <div class="left-kontakt-panels">
                        8:00 – 18:00 Uhr<br/><br/>
                        8:00 – 16:30 Uhr<br/><br/>
                        8:00 – 14:00 Uhr
                    </div>
                    <div class="clearfix"></div>
                    <div class="left-kontakt-panels phones">
                        <embed src="<?php echo get_template_directory_uri(); ?>/svg/envelope-icon.svg" alt=""> <a href="mailto:info@laserzentrum.dental">info@laserzentrum.dental</a><br/><br/>
                        <embed src="<?php echo get_template_directory_uri(); ?>/svg/phone-icon.svg" alt=""> 04331 / 35 26 930<br/><br/>
                        <embed src="<?php echo get_template_directory_uri(); ?>/svg/fax-icon.svg" alt=""> 04331 / 35 26 940<br/><br/>
                    </div>
                </div>
            </div>
           <?php echo do_shortcode( '[contact-form-7 id="27" title="Kontakt"]' ); ?>
        </div>
    </section>
    <section class="map">
        <div class="google-maps">
            <!--
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2328.010234286583!2d9.670080716006877!3d54.3037697102652!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b3a5d3f92e4dbd%3A0x88930f01c777a145!2sKieler+Str.+17%2C+24768+Rendsburg%2C+Niemcy!5e0!3m2!1spl!2spl!4v1455044445567" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            -->
            <div id="map"></div>
        </div>
    </section>

 <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gmap3.min.js"></script>
 <script src="//maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>
<script>
              jQuery(document).ready(function(){
                jQuery("#map").gmap3({
                    map: {
                        options: {
                            center:[54.3012697, 9.6700754],
                            zoom:15,
                            mapTypeControl: false,
                            scrollwheel: false,
                            backgroundColor: 'white',
                            streetViewControl: false,
                            draggable: false,
                            styles: [{
                                stylers: [{
                                    saturation: -100
                                }]
                            }]
                        }
                    },
                    marker:{
                        latLng:[54.3038649,9.6719459],
                        options: {
                         icon: new google.maps.MarkerImage(
                           "<?php echo get_template_directory_uri(); ?>/img/icon-marker.png",
                           new google.maps.Size(28, 42, "px", "px")
                         )
                        }
                    }

                });
                 });
       </script>


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php 
get_footer();