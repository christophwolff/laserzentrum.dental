<?php
/*
 * Template Name: Leistungen
 *
 */

get_header(); ?>

<section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-10">
            <h2>Innovative Zahnheilkunde </h2>
        </header>
        <div class="col-xs-12 panel-separation">
            <h4>Die innovative Zahnheilkunde hat, ausser dem Namen, nicht mehr viel mit der Zahnheilkunde der 60er, 70er und 80er Jahre zu tun. Wissen und Techniken haben sich ransant entwickelt, ebenso wie die Möglichkeiten Zähne zu erhalten, zu schützen oder notfalls zu ersetzen.</h4>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 six-panels six-panels-buttons">
            <!-- TOP PANEL -->
                 <div class="col-md-4">
                    <h3>Keramikkrone ohne Abdruck</h3>
                    <p>Durch die CEREC-Methode (CEramic REConstruction) ist es uns möglich in einer einzigen Sitzung, ohne Abdrücke eine schöne, individuelle Keramikkrone anzufertigen und diese fest einzugliedern. </p>
                    <a href="/leistungen/cerec"><button class="btn btn-default">Mehr erfahren</button></a>
                 </div>
                <div class="col-md-4 panel-border">
                    <h3>Implantate</h3>
                    <p>Als künstilche Zahnwurzel aus Titian sind Implantate in vielen Fällen die erste Wahl bei der Versorgung nach Zahnverlust.</p>
                    <a href="/leistungen/implantate"><button class="btn btn-default">Mehr erfahren</button></a>
                </div>
                <div class="col-md-4">
                    <h3>Entfernung von Amalgam</h3>
                    <p>Wir sind eine amlagamfreie Praxis aus Überzeugung! Menschen mit Amalgamfüllungen sind ca. 4 – 5 mal so stark mit Quecksilber belastet wie Menschen ohne Zahnfüllungen. </p>
                    <a href="/leistungen/amalgam"><button class="btn btn-default">Mehr erfahren</button></a>
                 </div>
              <div class="clearfix"></div>
            <!-- TOP PANEL BUTTONS -->
            <div class="clearfix"></div>
            <!-- BOTTOM PANEL -->
            <div class="col-md-4">
                <h3>Wurzelkanalbehandlung</h3>
                <p>Mit der Hilfe eines OP-Mikroskopes, dem Einsatz von Laser und modernsten Spezialinstrumenten können wir feinste Kanäle im Zahninneren darstellen und bearbeiten.</p>
                <a href="/leistungen/"><button class="btn btn-default">Mehr erfahren</button></a>
             </div>
            <div class="col-md-4">
                <h3>Individual-Prophylaxe</h3>
                <p>Durch eine regelmäßige und individuelle Zahnreinigung können Karies und Zahnfleischentzündungen oftmals vermieden werden! </p>
                <a href="/leistungen/prophylaxe"><button class="btn btn-default">Mehr erfahren</button></a>
            </div>
            <div class="col-md-4">
                <h3>Zahnfleischbluten</h3>
                <p>Blutendes Zahnfleisch beim Zähneputzen ist ein Anzeichen für eine Parodontitis (eine entzündliche Erkrankung des Zahnhalteapparates) die zum Zahnverlust führen kann. </p>
                <a href="/leistungen/zahnfleischbluten"><button class="btn btn-default">Mehr erfahren</button></a>
            </div>

            <div class="clearfix"></div>
        </div>
    </section>
    <div class="full-size-photo " id="f-s-p-7">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">
        <!-- CATEGORY -->
        <div class="leistungen-cat">
            <div class="col-sm-9 panel-separation ">
                <h2 class="nopad-mobile">Ästhetische Zahnheilkunde</h2>
            </div>
            <div class="col-sm-12 panel-separation">
                <h4>Stellungskorrekturen und Aufhellen der Zähne, Micro-Abrasion bei Schmelzflecken, unsichtbare Mehrschicht-Füllungen, Laser-Korrekturen des Zahnfleisches, Gesamtsanierungen, kieferorthopädische Behandlung bei Erwachsenen mit fast unsichtbaren Schienen.</h4>
            </div>
            <div class="col-sm-12 six-panels six-panels-buttons-categories">
                <div class="col-md-4 panel-border-right">
                    <div class="col-sm-11 nopad-mobile"><h3>Bleaching</h3>
                        <p>Helle, strahlende Zähne signalisieren jugendliche Attraktivität und Dynamik. Durch ein schonendes Bleaching können wir Ihre Zähne aufhellen. </p>
                        <a href="/leistungen/bleaching"><button class="btn btn-default">Mehr erfahren</button></a>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="col-sm-10 col-md-offset-2 nopad-mobile">
                        <h3>Die transparente Zahnspange</h3>
                        <p>Die praktisch unsichtbaren Zahnspange bewegen Ihre Zähne Schritt für Schritt zu einem Lächeln, auf das Sie stolz sein werden.</p>
                        <a href="/leistungen/"><button class="btn btn-default">Mehr erfahren</button></a>
                    </div>
                </div>
                <div class="col-md-4 hidden-xs hidden-sm">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/leistungen2.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- CATEGORY -->
        <div class="leistungen-cat">
            <div class="col-sm-9 panel-separation">
                <h2 class="nopad-mobile">CMD &#038; Kopfschmerzen</h2>
            </div>
            <div class="col-sm-12 panel-separation">
                <h4>Kopfschmerzen, Migräne, Nacken- und Rückenverspannungen sowie Wirbelsäulenprobleme sind weit verbreitet und haben oft etwas mit den Zähnen zu tun.</h4>
            </div>
            <div class="col-sm-12 six-panels six-panels-buttons-categories">
                <div class="col-md-5 hidden-xs hidden-sm">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/leistungen3.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-4 col-md-offset-1">
                        <h3>Kopfschmerzen &#038; Zähne</h3>
                         <p>Fehlbelastungen im Kieferbereich können mit höchauflösenden Instrumenten wie der digitalen Bissanalyse und digitale Kiefergelenksvermessung sichtbar gemacht und behandelt werden.</p>
                        <a href="/leistungen/"><button class="btn btn-default">Mehr erfahren</button></a>
                 </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- CATEGORY -->
        <div class="leistungen-cat">
            <div class="col-sm-9 panel-separation">
                <h2 class="nopad-mobile">Expressbehandlung</h2>
            </div>
            <div class="col-sm-12 panel-separation">
                <h4>Viele Menschen- unter ihnen besonders viele Selbstständige- leben mit einem enormen Zeit- und Leistungsdruck. Wir bieten Ihnen einen optimal, angepassten Behandlungsplan.</h4>
            </div>
            <div class="cat-button col-sm-12">
                <a href="/leistungen/expressbehandlung"><button class="btn btn-default">Mehr erfahren</button></a>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- CATEGORY -->
        <div class="leistungen-cat">
            <div class="col-sm-9 panel-separation">
                <h2 class="nopad-mobile">Therapielaser</h2>
            </div>
            <div class="col-sm-12 panel-separation">
                <h4>Therapie-Laser sind Laser im sichtbaren und unsichtbaren Wellenlängenbereich. Sie gehören in vielen Fällen von chronischen Erkrankungen oder schlecht heilenden Wunden zu den erprobten Therapien der Wahl.</h4>
            </div>
            <div class="cat-button col-sm-12">
                <a href="/leistungen/therapielaser"><button class="btn btn-default">Mehr erfahren</button></a>
            </div>
            <div class="clearfix"></div>
        </div>


    </section>


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php
get_footer();