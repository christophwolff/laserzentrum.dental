<?php
/*
 * Template Name: Wurzelbehandlung
 *
 */

get_header();
?>
<section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-9">
            <h2>Wir möchten <strong>Ihre Zähne erhalten</strong> und optimal versorgen!</h2>
        </header>
        <div class="col-sm-10">
            <p>Eine Wurzelkanalbehandlung ist notwendig wenn sich die Kariesbakterien bis zum Zahnnerv ausgebreitet haben. Aber auch Traumata können Ursache für eine Entzündung des Zahnnerves sein. Durch eine gezielte Entfernung des Nerves und Aufbereitung des Kanalsystems können wir Ihren Zahn langfristig erhalten!</p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 panel-separation">
            <h4>Die Erfolgsquote von herkömmlichen Wurzelkanalbehandlungen liegt bei 55%. Durch den Einsatz modernster Methoden und dem entsprechenden Know-How kann die Erfolgsquote auf über 90% gesteigert werden. </h4>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 op-mikroskop">
            <h2>Mit dem OP-Mikroskop, Laser und speziellen <strong>hochflexiblen Instrumenten</strong> erreichen wir dieses Ziel!</h2>
        </div>
    </section>
    <div class="bleaching full-size-photo " id="f-s-p-3">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">
        <div class="col-sm-12 three-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>OP-Mikroskop</h3>
                <p>Damit eine Wurzelbehandlung erfolgreich ist, müssen alle Kanäle aufgefunden & behandelt werden. Zum Teil sind diese Kanäle so fein wie ein Haar. Die hohe Vergrößerung ermöglicht uns diese zu finden.</p>
             </div>
            <div class="col-md-4">
                <h3>Laser</h3>
                <p>Die zum desinfizieren der Kanäle verwendete Spüllösung dringt bis zu 100 µm tief in das Gewebe ein. Mit dem Laser können wir 10 mal tiefer das Gewebe desinfizieren! Zudem werden spüllösungsresistente Bakterien durch den Laser abgetötet!  </p>
            </div>
            <div class="col-md-4">
                <h3>Hochflexible Spezialinstrumente</h3>
                <p>Nur mit Hilfe dieser Instrumente lassen sich die haarfeinen Kanäle bearbeiten und erkranktes Gewebe in ausreichendem Maße entfernen.</p>
            </div>
        </div>
        <div class="col-sm-10 panel-separation">
            <h2>Wie sieht die Abfolge einer Wurzelkanalbehandlung aus?</h2>
        </div>
        <div class="col-xs-10 circles-ul circles-bottom-padding">
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">1</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Schaffung von sauberen Verhältnissen</p>
                        <p>Zunächst wird der Zahn mit einem Gummi-OP Tuch von der Mundhöhle abgedichtet, so dass kein Speichel oder Bakterien die Kanäle verunreinigen können. Unter einer örtlichen Betäubung wird dann die Karies entfernt. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">2</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Auffinden und Reinigen der Kanäle</p>
                        <p>Mit Hilfe des OP-Mikroskops werden die Kanäle aufgesucht.  Das entzündete Gewebe wird anschließend mit hochflexiblen Instrumenten entfernt. Die Kanäle werden mit dem Laser desinfiziert & ein Medikament wird in das Kanalsystem appliziert. </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">3</div>
                </div>
                <div class="col-sm-9 with-ul col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Entgültige Füllung der Kanäle</p>
                        <p>In einer zweiten Sitzung werden die Kanäle noch einmal mit dem Laser desinfiziert und anschließend mit einem warmen def. Material (Guttapercha) abgefüllt. Mit dieser Technik können auch kleine Seitenkanäle versiegelt werden. Um den Zahn langfristig zu schützen ist eine Krone / Teilkrone notwendig. </p>
                    </div>
                </div>
            </div>
        </div>

    </section>


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();