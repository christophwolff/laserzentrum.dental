<!--NAVIGATION-->
    <?php 
    $is_grey_navbar = get_field('grey_navbar');
        if($is_grey_navbar  == 1): 
            $grey_class = "grey-navbar";
        endif;

        if(is_page('news')){
            $grey_class = "grey-navbar";
        }
    ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <?php echo get_template_part( 'templates/template-parts/content', 'nav-header' ); ?>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right <?php echo $grey_class ?>">
                    <li <?php if (is_page('lasermedizin')) echo 'class="active"';?>><a href="/lasermedizin">Lasermedizin</a></li>
                    <li <?php if (is_page('schlafmedizin')) echo 'class="active"';?>><a href="/schlafmedizin">Schlafmedizin</a></li>
                    <li class="dropdown dropdown-pc hidden-xs hidden-sm <?php if ($post->post_parent == '48' || is_page('leistungen')) echo "active";?>">
                        <a href="/leistungen" class="dropdown-toggle" role="button">Leistungen</a>
                        <div class="dropdown-menu">
                            <div class="container-fluid">
                                <div class="col-md-6">
                                    <span class="dropdown-header">Innovative Zahnheilkunde</span>
                                    <ul class="dropdown-two-columns">
                                        <li <?php if (is_page('cerec')) echo 'class="active"';?>><a href="/leistungen/cerec">Keramikkrone ohne Abdruck</a></li>
                                        <li <?php if (is_page('wurzelkanalbehandlung')) echo 'class="active"';?>><a href="/leistungen/wurzelkanalbehandlung">Wurzelkanalbehandlung</a></li>
                                        <li <?php if (is_page('Implantate')) echo 'class="active"';?>><a href="/leistungen/implantate">Implantate</a></li>
                                        <li <?php if (is_page('prophylaxe')) echo 'class="active"';?>><a href="/leistungen/prophylaxe">Individual-Prophylaxe</a></li>
                                        <li <?php if (is_page('amalgam')) echo 'class="active"';?>><a href="/leistungen/amalgam">Entfernung von Amalgam</a></li>
                                        <li <?php if (is_page('zahnfleischbluten')) echo 'class="active"';?>><a href="/leistungen/zahnfleischbluten">Zahnfleischbluten</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <span class="dropdown-header">Ästhetitik</span>
                                    <ul class="dropdown-one-column">
                                        <li <?php if (is_page('bleaching')) echo 'class="active"';?>><a href="/leistungen/bleaching">Bleaching</a></li>
                                        <li <?php if (is_page('therapielaser')) echo 'class="active"';?>><a href="/therapielaser">Perfektes Lächeln</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="dropdown-one-column superlinks">
                                        <li class="/leistungen/superlink"><a href="/kopfschmerzen">CMD</a></li>
                                        <li class="/leistungen/superlink"><a href="/therapielaser">Therapielaser</a></li>
                                        <li class="/leistungen/superlink"><a href="/expressbehandlung">Expressbehandlung</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown dropdown-mobile visible-xs visible-sm">
                        <a href="/leistungen" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Leistungen</a>
                        <div class="dropdown-menu">
                            <div class="container-fluid">
                                <div class="col-md-6">
                                    <span class="dropdown-header">Innovative Zahnheilkunde</span>
                                    <ul class="dropdown-two-columns">
                                        <li <?php if (is_page('cerec')) echo 'class="active"';?>><a href="/cerec">Keramikkrone ohne Abdruck</a></li>
                                        <li <?php if (is_page('')) echo 'class="active"';?>><a href="/wurzelkanalbehandlung">Wurzelkanalbehandlung</a></li>
                                        <li <?php if (is_page('implantate')) echo 'class="active"';?>><a href="/implantate">Implantate</a></li>
                                        <li <?php if (is_page('prophylaxe')) echo 'class="active"';?>><a href="/prophylaxe">Individual-Prophylaxe</a></li>
                                        <li <?php if (is_page('amalgam')) echo 'class="active"';?>><a href="/amalgam">Entfernung von Amalgam</a></li>
                                        <li <?php if (is_page('zahnfleischbluten')) echo 'class="active"';?>><a href="/zahnfleischbluten">Zahnfleischbluten</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <span class="dropdown-header">Ästhetitik</span>
                                    <ul class="dropdown-one-column">
                                        <li <?php if (is_page('bleaching')) echo 'class="active"';?>><a href="/bleaching">Bleaching</a></li>
                                        <li <?php if (is_page('therapielaser')) echo 'class="active"';?>><a href="/therapielaser">Perfektes Lächeln</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="dropdown-one-column superlinks">
                                        <li class="superlink"><a href="/kopfschmerzen">CMD</a></li>
                                        <li class="superlink"><a href="/therapielaser">Therapielaser</a></li>
                                        <li class="superlink"><a href="/expressbehandlung">Expressbehandlung</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li <?php if (is_page('philosophie')) echo 'class="active"';?>><a href="/philosophie">Philosophie</a></li>
                    <li <?php if (is_page('team')) echo 'class="active"';?>><a href="/team">Team</a></li>
                    <li <?php if (is_page('news')) echo 'class="active"';?>><a href="/news">News</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
    </nav>