<section class="blue-panel">
    <div class="container-fluid">
             <div class="col-md-12">
                <h2>Jetzt Termin vereinbaren</h2>
            </div>
            <div class="col-md-5 col-sm-8">
                <p>Gerne empfangen wir Sie persönlich zu einem Beratungsgespräch oder beantworten erste Fragen am Telefon.</p>
            </div>
             <div class="col-md-12 blue-buttons">
                    <a class="btn btn-white still-260" href="/kontakt">
                        Kontakt
                    </a>
                    <a class="btn btn-dark-blue still-260" href="/impressum">
                        Weitere Informationen
                    </a>
             </div>
     </div>
</section>
