
<div class="navbar-header <?php if (is_page()) echo "grey-navbar"; ?>">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <?php // Change Logo in favor of the background image ?>
    <?php if(get_field('grey_navbar') == 1 || is_home() || is_single()) { ?>

        <a class="navbar-brand" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-grey.png" alt="Laserzentrum Dental"></a>

    <?php } else { ?>
        
        <a class="navbar-brand" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Laserzentrum Dental"></a>

    <?php } ?>
 </div>