<?php
/*
 * Template Name: Zahnfleischbluten
 *
 */

get_header();
?>

<section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-10">
            <h2>Zahnfleischbluten kann ein Anzeichen für <strong>eine beginnende Parodontitis</strong> sein!</h2>
        </header>
        <div class="col-sm-10">
            <p>Eine Parodontitis ist eine entzündliche Erkrankung des Zahnhalteapparates, die in erster Linie durch Bakterien in den Zahnbelägen (Plaque) verursacht wird. Im Laufe der Erkrankung kommt es dabei zur Zerstörung des Zahnhalteapparates und damit zu Zahnfleischrückgang, Lockerung des Zahnes und in fortgeschrittenen Stadium auch zum Zahnverlust. Eine Parodontitis verläuft zumeist ohne Schmerzen und wird daher oft erst in einem fortgeschrittenen Stadium entdeckt.</p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-10 panel-separation">
            <h4>Eine nicht behandelte Parodontitis geht mit einer erhöhten Wahrscheinlichkeit für Herzerkrankungen, Diabetes und Risikoschwangerschaften einher.</h4>
        </div>
        <div class="col-xs-12 col-sm-8 wie-wird">
            <h2>Wie wird eine Parodontitis therapiert?</h2>
        </div>
        <div class="col-sm-10">
            <h4>Grundlage jeder Behandlung ist dabei die Beseitigung der verursachenden bakteriellen Beläge!</h4>
        </div>
    </section>
    <div class="full-size-photo " id="f-s-p-2">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">
        <div class="col-sm-12 three-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Vorbeugung ist die beste Therapie</h3>
                <p>Durch eine regelmäßige professionelle Zahnreinigung und einer effizienten Mundhygiene kann die Entstehung einer Parodontitis verhindert werden. </p>
            </div>
            <div class="col-md-4">
                <h3>Laser als Schlüssel zu erfolgreichen Therapie</h3>
                <p>Neben einer mechanischen Entfernung der Beläge nimmt der Laser eine entscheidende Rolle in der Therapie ein. Der Laser ermöglicht eine gezielte Eliminierung von besonders schädlichen Bakterien. So können wir in den meisten Fällen auf eine Antibiotikagabe verzichten! </p>
            </div>
            <div class="col-md-4">
                <h3>Eine genaue Diagnose ist für eine erfolgreiche Therapie entscheidend!</h3>
                <p>Wir messen die Taschen mit einer digitalen, kalibrierten Messsonde der Florida Probe! Somit erhalten wir einen detaillierten Befund und können die individuelle Therapie genauer planen und durchführen.</p>
            </div>

        </div>
        <div class="col-xs-12 col-sm-9 panel-separation">
            <h4>Die Nachsorge (Recall) ist für einen <strong>langfristen Erfolg der Therapie</strong> entscheidend! </h4>
        </div>
        <div class="col-xs-12 col-sm-9 panel-separation">
            <p>Im Rahmen der Nachsorgeuntersuchungen werden Zähne und Zahnfleisch kontrolliert und professionell gereinigt. Die Häufigkeit der Nachsorgetermine richtet sich nach dem Schweregrad der Erkrankung und dem individuellen Erkrankungsrisiko.
<br/>Ohne eine regelmäßige Nachsorge besteht die Gefahr, dass die Erkrankung zurückkehrt und die Zerstörung des Zahnhalteapparates weiter voranschreitet.</p>
        </div>
        <div class="col-xs-12 col-sm-9 panel-separation">
            <h4>Unterstützend wenden wir im Laserzentrum die <strong>antimikrobiellen Photodynamischen</strong> Therapie an.</h4>
        </div>
        <div class="col-xs-12 col-sm-9 panel-separate">
            <p>Durch Spülen der Zahnfleischtaschen mit einer Farblösung werden sämtliche Keime in der Zahnfleischtasche markiert. Mit einem zweiten Spülvorgang wird überschüssige Farblösung entfernt, so dass nur noch die mit Farbe markierten Bakterien verbleiben. Nun wird die Tasche mit einer speziellen Laserwellenlänge bestrahlt. Durch die Bestrahlung der an den Bakterien anhaftenden Farbmoleküle werden die Bakterien sofort abgetötet. Diese Behandlung  ist absolut schmerzfrei und greift  kein gesundes Gewebe an. </p>
        </div>
    </section>
<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();