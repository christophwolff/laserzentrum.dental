<?php
/*
 * Template Name: Expressbehandlung
 *
 */

get_header(); ?>

<section class="container-fluid leistungen">
        <header class="col-sm-10"><h2><strong>Die zeitsparende Alternative für Selbständige</strong> und andere Menschen mit vollem Terminkalender</h2></header>
        <div class="col-sm-10">
            <p>Viele Menschen leben mit einem enormen Zeit- und Leistungsdruck. Lang anstehende, teilweise auch umfangreiche zahnärztliche Behandlungen können oft aus Zeitgründen nicht angefangen oder weitergeführt werden. Dies verschlimmert die zahnmedizinische Situation und den Behandlungsdruck.
            <br/><br/>
            Wir haben dieses Dilemma unserer Patienten erkannt und bieten Ihnen deshalb die Möglichkeit der "Express-Behandlung" an.</p>
        </div>
        <div class="col-sm-9 panel-separation">
            <h4>Die Behandlung erfolgt – ohne Abstriche in der Qualität – zu besonderen Terminen. </h4>
        </div>
        <div class="col-sm-10">
            <p>Diese Express-Termine stehen in der Regel Freitags von 9–13:00 Uhr zur Verfügung.
            <br/><br/>
            Bitte lassen Sie uns gleich bei der Anmeldung bzw. bei unserem ersten Gespräch wissen, ob Sie an einer "Express-Behandlung" interessiert sind. Wir freuen uns, wenn wir Ihnen helfen können.</p>
        </div>

</section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php
get_footer();