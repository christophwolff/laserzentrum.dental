<?php
/*
 * Template Name: Laser
 *
 */

get_header();
?>

 <!-- BANNER -->
    <section class="huge-top laserbaner">
             <div class="down-border"></div>
                <div class="container-fluid ">
                    <div class="col-md-3 laser-photo col-sm-1 col-xs-2">
                        <div class="dot-white dot dot-top"></div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/laser.png" alt="">
                    </div>
                    <div class="col-md-9 col-sm-11 col-xs-10 nopad-mobile">
                        <h2>Nur wenige Zahnarztpraxen haben sich bisher dazu entschieden mit dieser <strong>innovativen Technik</strong> zu behandeln. Obwohl sie massive <strong>Vorteile bietet</strong>.</h2>
                        <div class="col-md-3 circle-chart chart-5">
                            <div class="inside">
                                5%
                            </div>
                        </div>
                        <div class="col-md-8 nopad-mobile">
                            <p>Nur 5% der Zahnärzte in Deutschland behandeln mit neuester Lasertechnologie</p>
                        </div>
                    </div>
                </div>
     </section>
    <!-- WHITE PANEL -->
    <section class="laser-white">
        <div class="container-fluid">
            <div class="col-md-3 col-sm-1 col-xs-2">
                <div class="dot-grey dot-long dot dot-bottom" id="dash-long"></div>
            </div>
            <div class="txt col-md-9 col-sm-11 col-xs-10 nopad-mobile" id="dash-height">
                <h4>Warum Laserbehandlung <strong>Schmerzfrei</strong> ist</h4>
                <p>Der WaterLase© Laser verwendet ein einzigartiges Zusammenspiel aus einer speziellen Wellenlänge des Laserlichtes und eines Wasser- bzw. Luftsprühsystems. Dadurch schneidet und fräst er präzise und vor allen Dingen kontaktlos, ohne Hitzeentwicklung, ohne Vibation und ohne Druck.</p>
            </div>
            <div class="clearfix"></div>
            <div class="dash-text first-dash-text">
                <div class="col-md-3 col-sm-1 col-xs-2">
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-md-9 col-sm-11 col-xs-10 nopad-mobile">
                    <h4>Kontaktlose Behandlung</h4>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="dash-text">
                <div class="col-md-3 col-sm-1 col-xs-2">
                   <div class="dot-grey dot dot-bottom"></div>
               </div>
               <div class="col-md-9 col-sm-11 col-xs-10 nopad-mobile">
                   <h4>Keine Makrofrakturen durch Vibration</h4>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="dash-text">
                <div class="col-md-3 col-sm-1 col-xs-2">
                   <div class="dot-grey dot dot-bottom"></div>
               </div>
               <div class="col-md-9  col-sm-11 col-xs-10 nopad-mobile">
                   <h4>Keine Schäden durch Reibungshitze </h4>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="dash-text">
                <div class="col-md-3 col-sm-1 col-xs-2">
                   <div class="dot-grey dot dot-bottom"></div>
               </div>
               <div class="col-md-9 col-sm-11 col-xs-10 nopad-mobile">
                   <h4>Die Laserimpluse sind mit 50 Mikrosekunden schneller, als eine Nervenzelle reagieren kann</h4>
               </div>
               <div class="clearfix"></div>
            </div>

        </div>
    </section>
    <!-- GREY PANEL -->
    <section class="laser-grey">
        <div class="doctor">
            <img src="<?php echo get_template_directory_uri(); ?>/img/doctor.png" class="img-responsive" alt="">
        </div>
        <div class="container-fluid">
            <div class="col-md-11">
                <h4>Laser ist nicht gleich Laser. </h4>
                <h4>Für unterschiedliche Behandlungen  werden unterschiedliche Laserwellenlängen benötigt. Um unseren Patienten die größtmögliche Anwendungsbreite anzubieten, können wir in unserem Laserzentrum auf <strong>sieben verschiedene</strong> Laser zurückgreifen.</h4>
            </div>
            <div class="hidden-xs">
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Low-Level-Laser-Therapie</p>
                            <p class="size">Wellenlänge: 500 – 900 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Biostimulation von Gewebe und Förderung der Wundheilung</p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Photodynamische Therapie</p>
                            <p class="size">Wellenlänge: 660 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Selektive Eliminierung von zuvor angefärbten Bakterien durch den Laser </p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Gewebedesinfektion mit dem Laser</p>
                            <p class="size">Wellenlänge: 810 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Keimreduktion im Wurzelkanal oder von Zahnfleischtaschen</p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Laser für Weichgewebschirurgie</p>
                            <p class="size">Wellenlänge: 810 & 980 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Schonung von Gewebe und damit weniger postoperative Schmerzen</p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Diagnostik durch Laserfluoreszenz</p>
                            <p class="size">Wellenlänge: 655 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Röntgenfreie Diagnose von Karies</p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Laserunterstützte Aufhellung von Zähnen</p>
                            <p class="size">Wellenlänge: 810 – 980 nm </p>
                        </div>
                        <div class="blue-part">
                            <p>Schonende Aufhellung der Zahnhartsubstanz gegenüber konventionellen Methoden</p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 blue-box">
                    <div class="col-md-12 blue-box-inside">
                        <div class="grey-part">
                            <p class="name">Lasergestützte Kavitätenpräparation</p>
                            <p class="size">Wellenlänge: ca 2780nm</p>
                        </div>
                        <div class="blue-part">
                            <p>Selektive Kariesentfernung unter Schonung von gesundem Gewebe </p>
                            <button class="btn btn-dark-blue">Mehr erfahren</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid nopad">
            <div class="visible-xs  nopad">
                <div id="slider">
                <button class="control_next btn"><i class="fa fa-angle-right"></i></button>
                <button class="control_prev btn"><i class="fa fa-angle-left"></i></button>
                <ul>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Low-Level-Laser-Therapie</p>
                                <p class="size">Wellenlänge: 500 – 900 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Biostimulation von Gewebe und Förderung der Wundheilung</p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Photodynamische Therapie</p>
                                <p class="size">Wellenlänge: 660 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Selektive Eliminierung von zuvor angefärbten Bakterien durch den Laser </p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Gewebedesinfektion mit dem Laser</p>
                                <p class="size">Wellenlänge: 810 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Keimreduktion im Wurzelkanal oder von Zahnfleischtaschen</p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Laser für Weichgewebschirurgie</p>
                                <p class="size">Wellenlänge: 810 & 980 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Schonung von Gewebe und damit weniger postoperative Schmerzen</p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Diagnostik durch Laserfluoreszenz</p>
                                <p class="size">Wellenlänge: 655 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Röntgenfreie Diagnose von Karies</p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Laserunterstützte Aufhellung von Zähnen</p>
                                <p class="size">Wellenlänge: 810 – 980 nm </p>
                            </div>
                            <div class="blue-part">
                                <p>Schonende Aufhellung der Zahnhartsubstanz gegenüber konventionellen Methoden</p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="col-md-3 col-sm-4 blue-box">
                        <div class="col-md-12 blue-box-inside">
                            <div class="grey-part">
                                <p class="name">Lasergestützte Kavitätenpräparation</p>
                                <p class="size">Wellenlänge: ca 2780nm</p>
                            </div>
                            <div class="blue-part">
                                <p>Selektive Kariesentfernung unter Schonung von gesundem Gewebe </p>
                                <button class="btn btn-dark-blue">Mehr erfahren</button>
                            </div>
                        </div>
                    </div></li>
                </ul>
                </div>
            </div>
        </div>
        </div>
        <div class="container-fluid">
            <div class="col-lg-11 col-lg-offset-1 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 col-xs-8 col-xs-offset-4 doctor-quote">
                <h4><span>„</span>Durch die <strong>fundierte Ausbildung</strong> und die <strong>jahrelange Erfahrung</strong> in der Anwendung zählen wir zu den <strong>wenigen Spezialisten</strong>, die die <strong>Vorteile der Laserbehandlung</strong> in der Zahnheilkunde nutzen.<span>“</span></h2>
                    <p>Dr . David Riha</p>
            </div>
            <div class="col-lg-7 col-lg-offset-4 col-md-6 col-md-offset-5 col-sm-7 col-sm-offset-5 hidden-xs laser-list">
                <h3>Das Laserzentrum Zahnheilkunde ist Mitglied folgender Fachverbände:</h3>
                <ul>
                    <li>Deutsche Gesellschaft für Parodontologie</li>
                    <li>Akademie Praxis und Wissenschaft</li>
                    <li>Academy of Laser Dentistry</li>
                    <li>European Society of Oral Laser Application</li>
                    <li>Academy for Computerized Dentistry of North America</li>
                    <li>Deutsche Gesellschaft für Computergestützte Zahnheilkunde</li>
                    <li>American Academy for Dental Sleep Medicine</li>
                    <li>Deutsche Gesellschaft für zahnärztliche Schlafmedizin</li>
                    <li>Deutsche Gesellschaft für Zahnärztliche Implantologie</li>
                </ul>
            </div>
        </div>
    </section>
    <div class="col-xs-12 visible-xs laser-list">
        <h3>Das Laserzentrum Zahnheilkunde ist Mitglied folgender Fachverbände:</h3>
        <ul>
            <li>Deutsche Gesellschaft für Parodontologie</li>
            <li>Akademie Praxis und Wissenschaft</li>
            <li>Academy of Laser Dentistry</li>
            <li>European Society of Oral Laser Application</li>
            <li>Academy for Computerized Dentistry of North America</li>
            <li>Deutsche Gesellschaft für Computergestützte Zahnheilkunde</li>
            <li>American Academy for Dental Sleep Medicine</li>
            <li>Deutsche Gesellschaft für zahnärztliche Schlafmedizin</li>
            <li>Deutsche Gesellschaft für Zahnärztliche Implantologie</li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <!-- WHITE PANEL -->
    <section class="laser-bottom">
        <div class="container-fluid">
            <div class="col-md-10"><h2>Pioniergeist und <strong>stetige Weiterbildung</strong> machen uns zum Experten</h2></div>
            <div class="col-md-12"><h4>Um auf den auf dem neusten Stand der Wissenschaft zu praktizieren absolvierte Dr. Riha 2015 das "Curriculum Laserzahnheilkunde" der Laserakademie Dr. Wittschier & des Deutschen Zentrums für orale Implantologie.</h4></div>
            <div class="col-md-10">
                <p>Dr.Stieve hat seine Zertifizierung zum Laserspezialisten im Jahr 2005 und den Fellow of the Academy 2009 bei der Academy of Laser Dentistry ALD, USA, abgelegt und ist ein erfahrener Laseranwender und Kursleiter.</p>
                <p>Wir halten im In-und Ausland Vorträge zu den Themen <u>CEREC</u>, <u>Funktionstherapie</u>, <u>Laser in der Zahnheilkunde</u>, Integration moderner Behandlungen in die tägliche Praxis sowie über betriebswirtschaftliche Praxisführung gehalten. Weiter sind wir für die Industrie und Firmen aus dem Dentalbereich beratend tätig.</p>
                <p>Für interessierte Kollegen bieten wir Laserkurse und Hands-on-Workshops aller Wellenlängen an. Speziell mit den Geräten der Firmen Lumenis und Elexxion bieten wir Praxishospitationen zu den verschiedenen Einsatzbereichen der unterschiedlichen Lasersysteme an. Unsere letzte Neuerwerbung ist ein Biolase I-Lase.</p>
                <button class="btn btn-default">Aktuelle Weiterbildungen</button>
            </div>
        </div>
    </section>
    <!-- BLUE-PANEL -->


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php
get_footer();