<?php
/*
 * Template Name: Jobs
 *
 */

get_header();
?>

<section class="container-fluid news-all">

  		<?php get_template_part('loop-jobs'); ?>

		<?php get_template_part('pagination'); ?>

	</section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php
get_footer();