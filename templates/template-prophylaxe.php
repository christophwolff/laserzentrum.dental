<?php
/*
 * Template Name: Prophylaxe
 *
 */

get_header();

?>

<section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-10">
            <h2>Die <strong>effektivste Behandlung</strong> in der Zahnarztpraxis ist die professionelle Zahnreinigung. </h2>
        </header>
        <div class="col-sm-12 six-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Schutz vor Karies</h3>
                <p>Die gründliche Entfernung vom gefährlichen Biofilm (dentale Plaque) und von Zahnstein von den Zahnflächen verringert die Belastung der Zähne durch die Säureangriffe und damit das Kariesrisiko.</p>
             </div>
            <div class="col-md-4">
                <h3>Implantate & die Professionelle Zahnreinigung</h3>
                <p>Implantate bzw. implantatgetragener Zahnersatz erfordern eine regelmäßige professionelle Reinigung & gute Mundhygiene. So können Implantate ein Leben lang halten.</p>
            </div>
            <div class="col-md-4">
                <h3>Nachhaltiger Schutz</h3>
                <p>Nur durch eine individuelle, professionelle Zahnreinigung IPS kann der optimale Erfolg – Verhinderung von neuer Karies und Parodontitis - langfristig sichergestellt werden.</p>
            </div>
            <div class="clearfix"></div>
            <!-- BOTTOM PANEL -->
            <div class="col-md-4">
                <h3>Risiko durch Paradontitis</h3>
                <p>Das Risiko einer Frühgeburt ist bei einer Schwangeren mit einer Parodontitis (Zahnfleischerkrankung) um das 7-fache erhöht. Ein unnötiges Risiko für Mutter und Kind.</p>
             </div>
            <div class="col-md-4">
                <h3>Vorbeugung schwerwiegender Krankheiten</h3>
                <p>Ebenso ergeben sich bei Herz-Kreislauferkrankungen, Diabetes und vielen anderen Vorerkrankungen erhöhte Risiken, die durch gezielte, professionelle Prophylaxe deutlich gesenkt werden können.</p>
            </div>
            <div class="col-md-4">
                <h3>Mundhygiene</h3>
                <p>Die Prophylaxe ist der erste Schritt zur Behandlung von Zahnfleischentzündungen, Parodontitis und Mundgeruch. Sie erleichtert zusätzlich die häusliche Mundhygiene.</p>
            </div>
        </div>
    </section>
    <div class="full-size-photo " id="f-s-p-6">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">

        <div class="col-sm-9 panel-separation">
            <h2>Was gehört zu einer <strong>sinnvollen Prophylaxe-Sitzung</strong> unbedingt dazu?</h2>
        </div>
        <div class="col-sm-12 panel-separation">
            <h4>Eine sorgfältige Prophylaxe dauert in der Regel mindestens eine Stunde. Wir bieten Ihnen mit unserem Prophylaxe-System ein auf Ihre individuelle Situation angepasstes Programm an.</h4>
        </div>
        <div class="col-xs-10 circles-ul circles-bottom-padding">
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">1</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Vollständige Entfernung aller harten und weichen Ablagerungen auf den Zähnen und erreichbaren Wurzeloberflächen</p>
                        <p class="col-sm-9 nopad">Hierbei werden mit speziellen Instrumenten schonend und behutsam alle harten und weichen Zahnbeläge auf allen sichtbaren und erreichbaren Zahnflächen entfernt.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">2</div>
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Entfernung von oberflächlichen Verfärbungen</p>
                        <p class="col-sm-9 nopad">Tee, Kaffee, Rotwein und Tabakkonsum führen zu einer Verfärbung der Zähne. Durch die Entfernung werden Ihre Zähne wieder strahlend weiß.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">3</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Reinigung der erreichbaren Zahnwurzeloberflächen und der Zahnzwischenräume</p>
                        <p class="col-sm-9 nopad">Direkt nach der Entfernung der harten und weichen Beläge erfolgt eine Politur der Zahnflächen, damit sich Bakterien nicht so schnell wieder an die Zähne anlagern können. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">4</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Bewertung des Zahnfleisches nach eventuellen Entzündungszeichen</p>
                        <p class="col-sm-9 nopad">Zu einer gründlichen Prophylaxe gehört auch die Begutachtung Ihres Zahnfleisches. Ein rötlich, verändertes Zahnfleisch ist ein Anzeichen für eine Entzündung und Bedarf genauer Beobachtung und evt. zahnärztlicher Therapie.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">5</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-long"></div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Kontrolle, Nachreinigung und Fluoridierung</p>
                        <p class="col-sm-9 nopad">Nach der Politur erfolgt eine professionelle Fluoridbehandlung. Fluoride stärken die Zahnstruktur, machen sie weniger angreifbar für Säureattacken der Plaquebakterien und ermöglichen die Einlagerung von Mineralsalzen in die Zähne.
                        <br/>Damit wird ein Reparaturmechanismus in Gang gesetzt, der Mineralverluste wieder völlig ausgleichen kann.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">6</div>
                </div>
                <div class="col-sm-8 col-xs-9 nopad-right">
                    <div class="number-text">
                        <p class="title">Anleitung zur richtigen Mundhygiene</p>
                        <p class="col-sm-9 nopad">Wir geben Ihnen Tips, wie Sie Ihre Zähne und Zahnfleisch noch besser pflegen können.Die richtige Zahnpasta sowie Hilfsmittel sind entscheidend um auch zu Hause eine optimale Mundhygiene aufrecht erhalten zu können.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php

get_footer();