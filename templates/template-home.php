<?php
/*
 * Template Name: Hompage
 *
 */
get_header();
?>
<!-- MODAL -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
         <div class="modal-body">
            <div class="iframe-wraper">
                <iframe  src="https://www.youtube.com/embed/Pk3VboPML_M?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
         </div>
       </div>
     </div>
   </div>
<!-- BANNER -->
    <section class="huge-top mainbaner">
        <div class="gradient-top">
            <div class="down-border"></div>
            <div id="for-collapse" class="container-fluid ">
                <!-- QUOTE -->
                <div class="row ">
                    <div class="col-lg-9 col-md-9 col-sm-9 full-height-row">
                        <div class="quote">
                            <h1 class="white animated fadeInUp">„Eine <strong>schmerzfreie</strong>, <strong>ganzheitliche</strong> Behandlung mit <strong>modernsten Methoden</strong> ist unser Anspruch“</h1>
                            <div class="author animated fadeInUp">Dr. David Riha</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ARROW DOWN-->
            <div class="go-down-position">
                <a href="#scroll-target" id="scroll-element">
                    <div class="go-down">
                        <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri();?>/img/arrowdown.svg" />
                        </svg>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!-- 3 PANELS -->
    <section class="panels-3" id="scroll-target">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 text-left">
                    <div class="col-md-9 nopad-pc">
                        <svg class="panel-icons" height="60" width="55" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image x="0" y="0" height="60" width="55" xlink:href="<?php echo get_template_directory_uri();?>/img/ico_1.svg" />
                        </svg>
                        <h3>Schmerzfrei</h3>
                        <p>Wir sind stolz darauf, unsere Patienten mit den bestmöglichen Behandlungen und Therapien versorgen zu können. Aus diesem Grund verwenden wir auch nur qualitativ hochwertige Materialien in unserer Praxis und investieren laufend
                            in modernstes Equipment.</p>
                    </div>
                </div>
                <div class="col-md-4 text-left panel-border">
                    <div class="col-md-10 col-md-offset-1">
                        <svg class="panel-icons" height="60" width="55" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image x="0" y="0" height="60" width="55" xlink:href="<?php echo get_template_directory_uri();?>/img/ico_2.svg" />
                        </svg>
                        <h3>Fortschrittlich</h3>
                        <p>Durch intensive Fortbildung in etlichen Schwerpunkten sind wir nicht nur technisch auf dem neuesten Stand. Durch dieses Know How können wir eine besonders optimale und schonende Behandlung gewährleisten.</p>
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="col-md-10 col-md-offset-1">
                        <svg class="panel-icons" height="60" width="55" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image x="0" y="0" height="60" width="55" xlink:href="<?php echo get_template_directory_uri();?>/img/ico_3.svg" />
                        </svg>
                        <h3>Ganzheitlich & nachhaltig</h3>
                        <p>Unser Denk- und Behandlungsansatz sieht immer ganzheitlich den ganzen Menschen und die langfristig nachhaltigste Lösung – nicht das kurzfristige Ausbessern.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- QUOTE -->
    <section class="bigquote">
        <div class="bigquote-gradient">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" id="big-quote">
                        <h2>Unsere Reputation beruht auf der jahrzehntelangen Verfolgung dieser Prinzipien – <strong>von kassenpolitischen Vorgaben unbeirrt</strong>.</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 6-PANELS -->
    <section class="panels-6">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="light">Untersuchungs- und Behandlungsspektrum</h2>
                </div>
                <div class="col-md-4 text-left">
                    <div class="col-md-9 nopad-pc txt-box">
                        <h3>Innovative Zahnheilkunde</h3>
                        <p>Durch neuste Technik und stetige Weiterbildung können wir Sie auf dem aktuellen Stand der Wissenschaft behandeln.</p>
                    </div>
                    <div class="col-md-12 nopad-pc">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
                <div class="col-md-4 text-left panel-border">
                    <div class="col-md-10 col-md-offset-1 txt-box">
                        <h3>Ästhetitische Zahnheilkunde</h3>
                        <p>Schöne, gesunde Zähne und ein strahlendes Lächelen steigern nicht nur das Selbstbewusstein sondern signalisieren Vitalität, Kraft und Wohlbefinden.</p>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="col-md-10 col-md-offset-1 txt-box">
                        <h3>CMD & Kopfschmerzen</h3>
                        <p>Etliche Menschen leiden an den Beschwerden. Zähneknirschen, Schlafstörungen oder ein ungleichmäßiger Biss können die Ursache sein.</p>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
                <div class="col-md-4 text-left panel-top-border">
                    <div class="col-md-9 nopad-pc txt-box">
                        <h3>Schlafmedizin</h3>
                        <p>Daz gehören Schnarchen oder andere schlafbezogene Atemstörungen. Das Erkennen und die Behandlung gehört zu einer unserer Schwerpunkte! </p>
                    </div>
                    <div class="col-md-12 nopad-pc">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
                <div class="col-md-4 text-left panel-border">
                    <div class="col-md-10 col-md-offset-1 txt-box">
                        <h3>Individual Prophylaxe</h3>
                        <p>Die effektivste vorbeugende Behandlung in der Zahnarztpraxis ist die professionelle Zahnreinigung.</p>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="col-md-10 col-md-offset-1 txt-box">
                        <h3>Expressbehandlung</h3>
                        <p>Viele Menschen leben mit einem enormen Zeit- und Leistungsdruck. Wir bieten Ihnen einen optimal, angepassten Behandlungsplan.</p>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <a href="#" class="btn btn-default">Mehr erfahren</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- VIDEO -->
    <div class="video video-index">
        <div class="container-fluid">
            <div id="btn-vid">
                <a class="btn btn-blue" data-toggle="modal" data-target="#myModal">
                    <svg class="video-icon" height="18" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="18" width="18" xlink:href="<?php echo get_template_directory_uri();?>/img/play.svg" />
                    </svg>
                    Video abspielen
                </a>
            </div>
        </div>
    </div>
    <!-- RED-PANEL-->
    <section class="red-panel">
        <div class="container-fluid nopad-mobile">
                 <div class="col-md-7 separate-red">
                    <h3>Ankündigungen</h3>
                    <ul>
                      <?php 
                        //Get the 3 latest post
                        global $post;
                        $myposts = get_posts('numberposts=3');
                        foreach($myposts as $post) :
                            setup_postdata($post);
                        ?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                </div>
                  <div class="col-md-4 col-md-offset-1  ">
                    <h3>Sprechzeiten</h3>
                    <span class="white-big-txt">
                        Urlaubszeit: Geänderte Sprechstunden
                    </span>
                    <p class="timetable">In der Zeit <strong>vom 31.8. – 14.9. befindet sich Dr. Riha im Urlaub</strong>. Dr. Stieve versieht die Sprechstunden in dieser Zeit.</p>
                    <div class="left timetable"><strong>Mo. - Mi <br/>Do.<br/>Fr.</strong></div>
                    <div class="left timetable">8:00 - 18:00 Uhr<br/>8:00 - 18:00 Uhr<br/>8:00 - 18:00 Uhr</div>
                </div>
         </div>
    </section>

    <!-- DARK RED-PANEL -->
    <div class="dark-red-panel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 center-mobile">
                    <a class="btn btn-red" href="/news">
                        Alle News lesen
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php get_footer( ); ?>
