<?php
/*
 * Template Name: Therapielaser
 *
 */

get_header();
?>

 <section class="container-fluid leistungen">
        <header class="col-sm-10"><h2>Neben unseren chirurgischen Lasern setzen wir <strong>auch unterschiedliche Therapielaser</strong> ein</h2></header>
        <div class="col-sm-10">
            <p>Therapie-Laser sind Laser im sichtbaren und unsichtbaren Wellenlängenbereich. Sie gehören in vielen Fällen von chronischen Erkrankungen oder schlecht heilenden Wunden zu den erprobten Therapienverfahren. Während "scharfe Laser" meist zum schonenden Gewebeabtrag an Schleimhaut oder Zahnsubstanz eingesetzt werden haben unsere Therapielaser andere Aufgaben.</p>
        </div>
        <div class="col-sm-12 six-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Stärken des lokale Immunsystems</h3>
                <p>Aktivierung von Makrophagen (Fresszellen), welche eine zentrale Rolle bei der Initiation und Regulation von Abwehrreaktionen spielen.</p>
            </div>
            <div class="col-md-4">
                <h3>Verbesserung der Blut- und Lymphzirkulation</h3>
                <p>Die Stimulation des Stoffwechsels bewirkt eine verbesserte Zirkulation des Lymph- & Blutflusses. Eine geringe postoperative Schwellung ist die Folge. </p>
            </div>
            <div class="col-md-4">
                <h3>Verbesserung des Stoffwechsels (Metabolismus) der Zellen</h3>
                <p>Stimulation der Mitochondrien (Kraftwerke der Zelle). Dadurch haben die Zellen mehr Energie für regenerative Prozesse zur Verfügung.</p>
            </div>
            <!-- BOTTOM PANEL -->
            <div class="col-md-4">
                <h3>Verstärkung der Prostaglandin- und Endorphinausschüttung</h3>
                <p>Stimulation von schadensmindernden  sowie heilungsfördernden Hormonen.</p>
            </div>
            <div class="col-md-4">
                <h3>Verminderung der Schmerzempfindung</h3>
                <p>Verringerte Schmerzempfindung durch eine herabgesetzte Erregbarkeit der Nervenzellen.</p>
            </div>
            <div class="col-md-4">
                <h3>Beschleunigung der Wundheilung</h3>
                <p>Nach der Laserbestrahlung nimmt die Zellteilung zu. Dies führt zu einer besseren Regeneration der Zellen und damit zu einer schnelleren Wundheilung.</p>
            </div>
        </div>
        <div class="col-sm-12">
            <h4>Die Wirkung der Lasertherapie tritt je nach Anwendung sofort oder verzögert auf. </h4>
        </div>
</section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();