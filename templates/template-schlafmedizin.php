<?php
/*
 * Template Name: Schlafmedizin
 *
 */

get_header();

?>
<!-- BANNER -->
    <section class="huge-top-schlafmedizin slider">
        <div class="down-border"></div>
        <header id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators schlafmedizin">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- Wrapper for Slides -->
            <div class="carousel-inner schlafmedizin">
                <div class="item active">
                    <div class="main-patient">
                        <div class="container-fluid">
                            <div class="col-md-3 nopad">
                                <div class="to-left">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/patient-thumb.png" class="img-circle img-responsive center-block" alt="">
                                    <h3 class="text-center">Sandra A.</h3><h5 class="text-center">Patientin</h5>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">
                                <svg class="quote-svg visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote.svg" />
                                </svg>
                                <svg class="center-block quote-svg hidden-lg hidden-md" height="30" width="240" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="30" width="240"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote-rotate.svg" />
                                </svg>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-8">
                                <div>
                                    <h4 class="animate-this">„Ich bin endlich wieder ausgeschlafen! Die Kiefergelenk- und Nackenschmerzen sind kaum noch vorhanden.“</h4>
                                    <p>Seit Jahren hat Frau A. unter chronischem Schlafmangel, Nackenschmerzen und Kiefergelenkproblemen gelitten – alle bisherigen Behandlungenversuche waren nicht erfolgreich. Nach gründlicher Untersuchung und Erstellung eines Schlafprotokolls bekam sie eine spezielle Schiene zur Öffnung der oberen Atemwege während des Schlafes. Nach nur drei Wochen sind die Besserungen eingetreten.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                 </div>
                <div class="item">
                    <div class="main-patient">
                        <div class="container-fluid">
                            <div class="col-md-3 nopad">
                                <div class="to-left">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/patient-thumb.png" class="img-circle img-responsive center-block" alt="">
                                    <h3 class="text-center">Sandra A. </h3>
                                    <h5 class="text-center">Patientin</h5>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">
                                <svg class="quote-svg visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote.svg" />
                                </svg>
                                <svg class="quote-svg hidden-lg hidden-md" height="30" width="240" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="30" width="240"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote-rotate.svg" />
                                </svg>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-8">
                                <div>
                                    <h4>„Ich bin endlich wieder ausgeschlafen! Die Kiefergelenk- und Nackenschmerzen sind kaum noch vorhanden.“</h4>
                                    <p>Seit Jahren hat Frau A. unter chronischem Schlafmangel, Nackenschmerzen und Kiefergelenkproblemen gelitten – alle bisherigen Behandlungenversuche waren nicht erfolgreich. Nach gründlicher Untersuchung und Erstellung eines Schlafprotokolls bekam sie eine spezielle Schiene zur Öffnung der oberen Atemwege während des Schlafes. Nach nur drei Wochen sind die Besserungen eingetreten.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                 </div>
                <div class="item">
                    <div class="main-patient">
                        <div class="container-fluid">
                            <div class="col-md-3 nopad">
                                <div class="to-left">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/patient-thumb.png" class="img-circle img-responsive center-block" alt="">
                                    <h3 class="text-center">Sandra A. </h3>
                                    <h5 class="text-center">Patientin</h5>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">
                                <svg class="quote-svg visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote.svg" />
                                </svg>
                                <svg class="quote-svg hidden-lg hidden-md" height="30" width="240" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="30" width="240"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote-rotate.svg" />
                                </svg>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-8">
                                <div>
                                    <h4>„Ich bin endlich wieder ausgeschlafen! Die Kiefergelenk- und Nackenschmerzen sind kaum noch vorhanden.“</h4>
                                    <p>Seit Jahren hat Frau A. unter chronischem Schlafmangel, Nackenschmerzen und Kiefergelenkproblemen gelitten – alle bisherigen Behandlungenversuche waren nicht erfolgreich. Nach gründlicher Untersuchung und Erstellung eines Schlafprotokolls bekam sie eine spezielle Schiene zur Öffnung der oberen Atemwege während des Schlafes. Nach nur drei Wochen sind die Besserungen eingetreten.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                 </div>
                <div class="item">
                    <div class="main-patient">
                        <div class="container-fluid">
                            <div class="col-md-3 nopad">
                                <div class="to-left">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/patient-thumb.png" class="img-circle img-responsive center-block" alt="">
                                    <h3 class="text-center">Sandra A. </h3>
                                    <h5 class="text-center">Patientin</h5>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">
                                <svg class="quote-svg visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote.svg" />
                                </svg>
                                <svg class="quote-svg hidden-lg hidden-md" height="30" width="240" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                                    <image x="0" y="0" height="30" width="240"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/blue-quote-rotate.svg" />
                                </svg>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-8">
                                <div>
                                    <h4>„Ich bin endlich wieder ausgeschlafen! Die Kiefergelenk- und Nackenschmerzen sind kaum noch vorhanden.“</h4>
                                    <p>Seit Jahren hat Frau A. unter chronischem Schlafmangel, Nackenschmerzen und Kiefergelenkproblemen gelitten – alle bisherigen Behandlungenversuche waren nicht erfolgreich. Nach gründlicher Untersuchung und Erstellung eines Schlafprotokolls bekam sie eine spezielle Schiene zur Öffnung der oberen Atemwege während des Schlafes. Nach nur drei Wochen sind die Besserungen eingetreten.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                 </div>
            </div>
        </header>
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="10" width="18"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg" />
                    </svg>
                </div>
            </a>
        </div>
     </section>
    <!-- TECHONOLGIES -->
    <section id ="scroll-target">
        <div class="container-fluid">
                <div class="col-md-12 schnarchen">
                    <h4>Schnarchen wird heute in der Schlafmedizin als schlafbezogene Atemstörung bezeichnet und sollte wegen seiner weitreichenden gesundheitlichen Gefahren untersucht werden.</h4>
                </div>
                <div class="col-sm-12 three-panels no-min-height">
                    <!-- TOP PANEL -->
                    <div class="col-md-4">
                        <h3>Unterbrechung des Schlafs</h3>
                        <p>Einige Schnarcher produzieren einen Laut, der lauter ist als die Lautstärke, die nach Arbeitsschutzbestimmungen am Arbeitsplatz erlaubt ist.
                            <br/>Der Schlaf von Bettpartnern von Schnarchern wird im Schnitt 21 mal pro Nacht gestört im Vergleich zu 27 mal pro Nacht für den Schnarcher selbst. Bettpartner können das Schnarchen des anderen nicht verhindern. meisten Schnarcher werden von ihrem Bettpartner zum Arzt geschickt.</p>
                     </div>
                    <div class="col-md-4">
                        <h3>Schlafapnoe & Sekundenschlaf</h3>
                        <p>Je öfter jemand während der Woche schnarcht, umso wahrscheinlicher ist es, dass eine Tagesmüdigkeit mit Gefahr des "Sekundenschlafes" auftaucht. Es werden mehr Verkehrsunfälle durch Sekundenschlaf infolge obstruktiver Schlafapnoe (Atemaussetzer während des Schlafes) als durch Alkoholeinfluss verursacht.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>Leistungsabfall & Impotenz</h3>
                        <p>Schnarchen kann zu Leistungsabfall, Impotenz und zu erheblichen gesundheitlichen Risiken führen.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 schnarchen">
                    <h4>Schnarchen hat die Tendenz sich zu verschlimmern, wenn die Personen älter werden oder an Gewicht zunehmen, da sich die Luftpassagen im Rachen verengen.</h4>
                </div>

                <div class="col-md-4 text-center schlafmedizin-charts sch-chart">
                    <div class="sch-chart"><img src="<?php echo get_template_directory_uri(); ?>/img/chart-1.png" alt="" class="img-responsive center-block"></div>
                    <p class="big-paragraphs col-md-6 col-md-offset-3">
                        Männer, die schnarchen
                    </p>
                </div>

                <div class="col-md-4 text-center schlafmedizin-charts sch-chart">
                        <div class="sch-chart"><img src="<?php echo get_template_directory_uri(); ?>/img/chart-2.png" alt="" class="img-responsive center-block"></div>
                    <p class="big-paragraphs col-md-6 col-md-offset-3">
                        Frauen, die schnarchen
                    </p>
                </div>
                <div class="col-md-4 text-center schlafmedizin-charts sch-chart bottom-space">
                        <div class="sch-chart"><img src="<?php echo get_template_directory_uri(); ?>/img/chart-3.png" alt="" class="img-responsive center-block"></div>
                    <p class="big-paragraphs col-md-8 col-md-offset-2">
                        Männer über 40, die schnarchen
                    </p>
                </div>




                <div class=" schnarchen">
                    <h4 class="col-md-12">Bei der Behandlung von schlafbezogenen Atemstörungen und Schlafstörungen gelten die nachfolgenden Regeln als ein besonders wichtiger Baustein bei der Behandlung:</h4>
                    <p class="col-md-10 big-paragraphs top-space bottom-space">Koffein hat lang andauernde stimulierende Wirkung und kann somit den Schlaf beeinträchtigen. Deshalb ab nachmittags keine anregenden Getränke (Kaffee, Cola, schwarzen Tee) mehr trinken.
                        <br/><br/>
                        Vermeiden Sie abendlichen Alkoholgenuss („Schlummertrunk“)! Alkohol keinesfalls als Schlafmittel einsetzen! Alkohol kann das Einschlafen zwar beschleunigen, unterdrückt aber Tief- und REM-Schlaf und verzögert u.U. die „Aufweckreaktion“ bei Atemaussetzern.
                        <br/><br/>
                        Keine schweren Mahlzeiten am Abend! Durch erhöhte Magen- und Darmtätigkeit kann der Schlaf unruhiger und oberflächlicher werden. Besser eine leichte Abendmahlzeit, am besten kohlehydrat- und weizenfrei.
                        <br/><br/>
                        Regelmäßige körperliche Aktivität! Diese wirkt sich günstig auf den (Tief-)Schlaf aus. Jedoch keine extreme körperliche Aktivität in den Abendstunden, da dadurch die Körpertemperatur wieder ansteigt, was das Einschlafen beeinträchtigen könnte.
                        <br/><br/>
                        Allmähliche Verringerung geistiger und körperlicher Anstrengung vor dem Zubettgehen.
                        <br/><br/>
                        Keine Tätigkeiten am Bildschirm: TV, Laptop oder Handy
                        <br/><br/>
                        Ein persönliches Einschlafritual und regelmässige Bettzeiten (auch am Wochenende) fördern den Schlaf!
                        <br/><br/>
                        Das Schlafzimmer sollte eine angenehme Atmosphäre haben (richtige Temperatur, gut verdunkelbar, keine Lärmquellen, kein Elektrosmog).
                    </p>
                    <div class="clearfix"></div>
                    <div class="col-md-2 sleep-medicine">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/sleep-medicine.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-10 bottom-space">
                        <p class="big-paragraphs">Die Behandlung von schlafbezogenen Atemstörungen, zu denen auch das Schnarchen gehört, erfolgt bei uns nach den Richtlinien der beiden Fachgesellschaften American Academy for Denral Sleep Medicine AADSM und der Deutschen Gesellschaft für Zahnmedizinische Schlafmedizin DGZS.</p>
                    </div>
                </div>
         </div>
    </section>


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php

get_footer();