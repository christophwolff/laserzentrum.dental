<?php
/*
 * Template Name: Bleaching
 *
 */

get_header(); ?>

<section class="container-fluid leistungen">
        <header class=" col-sm-9 col-xs-12">
            <h2>Helle, strahlende Zähne signalisieren <strong>jugendliche Attraktivität und Dynamik</strong></h2>
        </header>
        <div class="col-sm-12 panel-separation">
            <h4>Oft ist es sinnvoll vor Beginn einer umfangreichen Sanierung eine helle Zahnfarbe für die eigenen Zähne festzulegen. Dann können Füllungen und Kronen dieser hellen Zahnfarbe angeglichen werden.</h4>
        </div>
        <div class="col-sm-12 three-panels">
            <div class="col-md-4">
                <h3>Steigerung der Attraktivität</h3>
                <p>Natürliche, ansonsten gesunde Zähne, haben die Tendenz im Laufe der Zeit grauer oder gelber zu wirken. Diese Farbveränderungen können durch geeignete Verfahren des Bleichens in vielen Fällen wieder rückgängig gemacht und das strahlende Weiß der jugendlichen Zähne wieder hergestellt werden.</p>
            </div>
            <div class="col-md-4">
                <h3>Nicht Schädlich</h3>
                <p>Das schonende Bleichen der Zähne ist nicht schädlich und kann bedenkenlos angewendet werden, wenn es zahnärztlich angeordnet und auf die individuellen Gegebenheiten angepasst durchgeführt wird. Allerdings sollte das Bleichen schonend und in kleinen Schritten durchgeführt werden.</p>
            </div>
        </div>
    </section>
         <div class="bleaching full-size-photo " id="f-s-p-1">
             <div class="go-down-position">
                 <a href="#scroll-target" id="scroll-element">
                     <div class="go-down">
                         <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                             <image x="0" y="0" height="10" width="18" xlink:href="img/arrowdown.svg"></image>
                         </svg>
                     </div>
                 </a>
             </div>
         </div>


    <section class="container-fluid leistungen leistungen-next" id="scroll-target">
        <div class="col-sm-10">
            <h2>Verschiedene Bleaching-Verfahren</h2>
        </div>
        <div class="col-sm-10 paragraph-h4-panels">
            <h4>Internal Beaching</h4>
            <p>Ist ein einzelner Zahn z.B. nach einem Unfall dunkel verfärbt, kann er in vielen Fällen durch "internal Bleaching", also durch Bleichen von innen wieder seine alte, helle Farbe erhalten. Hierbei wird nach Entfernen der meist vorhandenen Füllung der Zahn von innen wieder aufgehellt. Möglicherweise sind mehrere Sitzungen notwendig. Internal Bleaching ist völlig schmerzlos.</p>
            <h4>Micro-Abrasion</h4>
            <p>Oft stellen sich nach einer kieferorthopädischen Behandlung mit festen Apparaturen, oder auch ohne ersichtlichen Grund, an ansonsten makellosen Zähnen unschöne weiße Flecken, sog. "Fluorose", ein. Bei diesen "White Spots" handelt es sich meist um Mineralisationsstörungen, die völlig ungefährlich aber eben nicht ästhetisch sind.
                <br/><br/>
                Hier gibt es eine weithin unbekannte, in den USA entwickelte und von uns seit Jahren erfolgreich angewandte Methode, diese Flecken völlig schmerzlos und dauerhaft zu entfernen.</p>
            <h4>Home Bleaching</h4>
            <p>Unterstützend kann mit dieser Methode auch zu Hause mittels einer speziell angepassten Schiene und einem Spezial-Gel der Bleichvorgang wiederholt oder unterstützt werden. Wir wenden beide Verfahren in Abstimmung mit Ihnen an. </p>
        </div>
        <div class="col-sm-10">
            <h4>Die Auswahl des Bleichverfahrens muss allerdings durch einen erfahrenen Zahnarzt erfolgen, weil die speziellen Umstände im Patientenmund ein individuelles Vorgehen erfordern.</h4>
        </div>
        <div class="col-sm-7 col-xs-12 vorgehen">
            <h2>Vorgehen beim Bleaching</h2>
        </div>
        <div class="col-xs-12 circles-ul">
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">1</div>
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Abdecken des Zahnfleischs mit einer Schutzschicht</p>
                        <p>Das Bleichen mittels spezieller Lichtquellen oder Laser in der Praxis wird als "In-Office-Bleaching" bezeichnet. Hier wird das Zahnfleisch mit einer Schutzschicht abgedeckt. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">2</div>
                    <div class="dot-grey dot dot-bottom"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Auftragen eines Bleichgels</p>
                        <p>Danach wird ein Bleichgel aufgetragen und mit Laser oder speziellen Lampen aktiviert. Nach einigen Minuten der Einwirkzeit wird das Gel abgespült und neues Gel aufgetragen. Der Bleicheffekt ist sofort zu sehen.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">3</div>
                </div>
                <div class="col-sm-9 with-ul col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Schutz durch Fluoridierung</p>
                        <p>Nach Beendigung des Bleichens wird der Zahnfleischschutz entfernt und die Zähne zum Schutz fluoridiert. In seltenen Fällen muss eine Bleichsitzung wiederholt werden, um das gewünschte Ergebnis zu erreichen.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();