<?php
/*
 * Template Name: Philosophie
 *
 */

get_header(); ?>

<!-- BANNER -->
    <section class="huge-top slider">
        <div class="down-border"></div>
        <header id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                     <div class="fill" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/philosophie_carousel_1.jpg');"></div>
                </div>
                <div class="item">
                     <div class="fill" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/philosophie_carousel_2.jpg');"></div>
                </div>
                <div class="item">
                     <div class="fill" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/philosophie_carousel_1.jpg');"></div>
                </div>
                <div class="item">
                     <div class="fill" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/philosophie_carousel_2.jpg');"></div>
                </div>
            </div>
        </header>
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="10" width="18"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg" />
                    </svg>
                </div>
            </a>
        </div>
     </section>
    <!-- TECHONOLGIES -->
    <section class="panel-2-columns" id ="scroll-target">
        <div class="container-fluid">
                 <div class="col-lg-11">
                    <h2>Wir nutzen nur die <strong>neueste Technologie</strong> für unsere Patienten</h2>
                </div>
                <div class="col-md-12">
                    <ul class="left">
                        <li>Chirurgische Hart-und Weichgewebslaser & Therapie-Laser</li>
                        <li>Digitale intraorale und extraorale Kameras</li>
                        <li>Digitales Röntgen</li>
                        <li>Digitale Aufzeichnung von Kiefergelenkbewegungen – <i>Zebris</i></li>
                        <li>Digitales Scannen von Zahnfehlbelastungen – <i>T-Scan</i></li>
                        <li>Digitales Cerec 3D Verfahren und digitale Abdrücke</li>
                        <li>Mimimalinvasive Implantologie</li>
                    </ul>
                    <ul class="right">
                        <li>OP-Mikroskop und Lupenbrillen</li>
                        <li>Eigenes Meisterlabor</li>
                        <li>Digital, kalibrierte Messung von Zahnfleischtaschen – <i>Florida Probe</i></li>
                        <li>Qualitätsmanagement</li>
                        <li>Recall-System und Terminbestätigung</li>
                        <li>Zusammenarbeit mit Fachärzten und Physiotherapeuten</li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <h4>Moderne Zahnheilkunde hat nicht mehr viel mit der Zahnheilkunde der 60er, 70er und 80er Jahre zu tun. Wissen und Techniken haben sich explosionsartig entwickelt. Ebenso wie die Möglichkeiten <strong>Zähne zu erhalten und zu schützen.</strong></h4>
                </div>
         </div>
    </section>
    <!-- TESTIMONY -->
    <section class="philosophy testimony">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-md-11 col-sm-10 col-xs-12 testimony-up">
                   <div class="testimony">
                       <h2 class="white animate-this">„Ich habe <strong>keine Angst</strong> mehr <strong>vor dem Zahnarzt</strong>“</h2>
                       <div class="author animate-this"><strong>Romy Sander</strong>  – Patientin</div>
                   </div>
               </div>
            </div>
         </div>
    </section>
    <!-- RED PANEL -->
    <section class="red-panel red-count">
        <div class="container-fluid">
            <div class="row">
                <h2 class="">Unsere Leitlinien</h2>
                <div class="boxs">
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">1</div>
                        <p class="text-center col-md-11">Unsere Patienten stehen im Mittelpunkt, weil wir sie mögen</p>
                    </div>
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">2</div>
                        <p class="text-center col-md-11">Wir behandeln äußerst schonend, freundlich und kompetent</p>
                    </div>
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">3</div>
                        <p class="text-center col-md-11">Freiheit von Angst und Schmerzen </p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">4</div>
                        <p class="text-center col-md-11">Unsere Patienten stehen im Mittelpunkt, weil wir sie mögen</p>
                    </div>
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">5</div>
                        <p class="text-center col-md-11">Wir behandeln äußerst schonend, freundlich und kompetent</p>
                    </div>
                    <div class="col-md-4 red-6-panels">
                        <div class="number center-block text-center">6</div>
                        <p class="text-center col-md-11">Freiheit von Angst und Schmerzen </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- WHITE TEXT -->
    <section class="paragraphs">
        <div class="container-fluid">
                 <div class="col-md-9">
                    <h2>Wir behandeln alle unsere Patienten, als säßen wir selbst auf dem Behandlungsstuhl</h2>
                    <p>Eine absolut schonende, schmerzfreie Behandlung in ist Grundvoraussetzung für eine stressfreie, entspannte Atmosphäre. Deswegen werden alle Behandlungen, bei denen es schmerzhaft werden könnte, unter Lokalanästhesie durchgeführt.</p>
                    <h2>Wir sehen Sie in Ihrer Gesamtheit</h2>
                    <p>Wenn Zähne schmerzen, steckt oft mehr als nur ein kariöser Defekt dahinter. Zähnepressen, Knirschen, Fehlbelastung, Parodontitis oder versteckte Karies an einem benachbarten Zahn. Deswegen untersuchen wir Sie gründlich und versuchen immer, eine <strong>nachhaltige Lösung</strong> zu finden. </p>
                    <p>Gewissenhafte Zahnheilkunde hört nicht bei den Zähnen auf, aber <strong>viele gesundheitliche Probleme fangen im Mund</strong> an. Schlafbezogene Atemstörungen wie Schnarchen und nächtliche Atemaussetzer, unerkannte Verengung der oberen Atemwege oder die Behandlung chronischer Kopf-und Nackenschmerzen gehören – teilweise in Zusammenarbeit mit anderen Fachärzten – deshalb auch zu unserem Arbeitsgebiet.</p>
                    <h2>Behandeln ist gut, Vorbeugen noch besser</h2>
                    <p>Wir haben heute die Möglichkeit fast alle unangenehmen Situationen mit Zähnen zu verhindern. Wenn Sie sich in unsere Behandlung begeben, werden wir ein auf Sie abgestimmtes Pflege- und Erhaltungsprogramm empfehlen. Wir freuen uns wenn wir auch bei Ihnen die Zahnsubstanz langfristig erhalten können.</p>
                    <h2>Wir lieben Tranparenz</h2>
                    <p>Wir möchten, dass Sie verstehen können wie und warum bestimmte Dinge in einer sinnvollen Reihenfolge geschehen sollten. Wir vermitteln Ihnen auch den Wert dieser Massnahmen und wie diese Kosten entstehen. Deswegen werden wir vor allen kostenpflichtigen Behandlungsmaßnahmen mit Ihnen ein Kostengespräch führen und gemeinsam eine Lösung für Sie entwickeln.</p>
                </div>
        </div>
    </section>
    <!-- TESTIMONY -->
    <section class="philosophy-2 testimony">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-md-11 col-sm-10 col-xs-12 testimony-up">
                   <div class="testimony">
                       <h2 class="white animate-this">Vor jeder Behandlung findet ein <strong>ausführliches Beratungsgespräch statt</strong>.</h2>
                    </div>
               </div>
            </div>
         </div>
    </section>
<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();