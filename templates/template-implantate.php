<?php
/*
 * Template Name: Implantate
 *
 */

get_header(); ?>

    <section class="container-fluid leistungen">
        <header class=" col-xs-12 col-sm-9">
            <h2>Implantate sind in vielen Fällen <strong>die erste Wahl</strong> bei der Versorgung nach Zahnverlust.</h2>
        </header>
        <div class="col-sm-9">
            <p>Implantate verhindern unnötigen Knochenabbau und sind klinisch sehr erfolgreich, da sie ihre Funktion lange behalten. Durch den Einsatz von Implantaten kann, je nach Situation, oft ganz oder teilweise auf das Beschleifen gesunder Zähne verzichtet werden.<br/>
Noch vor einigen Jahren konnten nur dort Implantate gesetzt werden, wo auch genug Knochen vorhanden war. Heute können wir durch den Einsatz von speziellen Techniken wie "Sinus-Lift, "Knochenaufbau" und "Membrantechnik" sehr viel öfter das Implantat dort einpflanzen, wo ursprünglich ein Zahn stand. </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12 panel-separation">
            <h4><span class="underline">Herr Dr. Riha</span> nimmt seit 2013 regelmäßig an einer Intensivschulung (Curriculum Implantologie) der Deutschen Gesellschaft für Implantologie teil. Bei dieser deutschlandweiten  Fortbildung mit hochqualifizierte Referenten <strong>werden neueste Techniken erlernt und angewandt.</strong></h4>
        </div>
    </section>
    <div class="full-size-photo " id="f-s-p-4">
        <div class="go-down-position">
            <a href="#scroll-target" id="scroll-element">
                <div class="go-down">
                    <svg id="arrowdown" height="10" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image x="0" y="0" height="10" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/arrowdown.svg"></image>
                    </svg>
                </div>
            </a>
        </div>
    </div>
    <section class="container-fluid leistungen-next" id="scroll-target">
        <div class="col-sm-8">
            <h2>Wann sind Implantate sinnvoll?</h2>
        </div>
        <div class="col-sm-12 three-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Zur Sofortimplantation bei Verlust eines einzelnen Zahnes</h3>
                <p>Hier wird der zerstörte Zahn äusserst schonend, meist unter Laseranwendung, entfernt und ein passendes Implantat nach einer individuellen Abheilzeit in die entstandene Lücke gesetzt.</p>
             </div>
            <div class="col-md-4">
                <h3>Zur Vermeidung von Herausnehmbarem Zahnersatz</h3>
                <p>Oft fehlen ein oder zwei Pfeilerzähne um eine feste Brücke einsetzen zu können. Mit Implantaten können die fehlenden Pfeilerzähne eingesetzt werden. </p>
            </div>
            <div class="col-md-4">
                <h3>Zur Stabilisierung von schlecht sitzenden Prothesen</h3>
                <p>Viele Menschen leiden unter schlecht sitzenden Prothesen. Sie können oft nicht mehr alle Lebensmittel zu sich nehmen und trauen sich nicht in Gesellschaft essen, weil die schlecht sitzenden Prothesen dies nicht zulassen. Durch Implantate können in vielen Fällen Prothesen schnell und sicher wieder ihre volle Funktion erreichen.</p>
            </div>
        </div>
        <div class="col-sm-9 panel-separation">
            <p>Neben einigen Allgemeinerkrankungen können spezielle Umstände eine Behandlung mit Implantaten verbieten oder ungünstig erscheinen lassen. </p>
        </div>
        <div class="col-sm-9 quote-p">
            <div class="quote-p-p">
                <p>Bitte wenden Sie sich an uns und lassen sich in der Implantatsprechstunde beraten.</p>
            </div>
        </div>
        <div class="col-sm-6 wie-verlauft">
            <h2>Wie verläuft eine Implantation?</h2>
        </div>
        <div class="col-xs-10 circles-ul circles-bottom-padding">
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">1</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-longer"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Voruntersuchung</p>
                        <p>Vor jedem Eingriff erfolgt eine umfangreiche und sorgfältige Untersuchung der Hart- und Weichgewebe, sowie der Kieferfunktion. Gipsmodelle werden angefertigt, um die anatomische Situation vermessen zu können. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">2</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-long"></div>
                </div>
                <div class="col-sm-6 col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Vorbereitung des Implantats</p>
                        <p>Entsprechende Röntgen- und Operationsschablonen werden angefertigt, mit deren Hilfe die optimale Position des Implantates festgelegt wird. Schließlich wird ein in der Länge und im Durchmesser geeignetes Implantat ausgewählt, um Schäden an Nachbarstrukturen sicher zu vermeiden. Sie werden eingehend über mögliche Risiken und entstehende Kosten schriftlich und mündlich aufgeklärt.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">3</div>
                    <div class="dot-grey dot dot-bottom dot-bottom-long"></div>
                </div>
                <div class="col-sm-9 with-ul col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Einsetzen des Implantats</p>
                        <p>Der Eingriff selbst wird unter lokaler Betäubung durchgeführt. Anschließend wird das Implantatbett präpariert und das Implantat eingebracht. Je nach Zustand des Implantatlagers wird gegebenenfalls der Knochen zur Stabilisierung aufgebaut. Um eine störungsfreie Einheilung zu gewährleisten, wird das Implantat, je nach Implantat-Typ, anschließend mit Mundschleimhaut abgedeckt und vernäht. </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- SINGLE LI -->
            <div class="circles-li">
                <div class="for-numb-circ">
                    <div class="number-circle">4</div>
                </div>
                <div class="col-sm-9 with-ul col-xs-10 nopad-right">
                    <div class="number-text">
                        <p class="title">Einpassen des Zahnersatzes</p>
                        <p>Die Fäden werden ca. 7–10 Tage nach dem Eingriff entfernt. Bis zur endgültigen Versorgung, die meist 12 Wochen nach dem Ersteingriff erfolgt, darf das Implantat nicht belastet werden, um ein festes Anwachsen des Knochens zu erreichen. Nach dieser Einheilzeit wird das Implantat wieder freigelegt und mit entsprechenden Aufbauteilen ergänzt, an denen dann der geplante Zahnersatz befestigt wird.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    
<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();