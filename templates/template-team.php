<?php
/*
 * Template Name: Team
 *
 */

get_header(); ?>

<!-- BANNER -->
    <section class="huge-top teambaner">
             <div class="down-border"></div>
                <div id="for-collapse" class="container-fluid ">

                </div>

     </section>
    <!-- FIRST DOCTOR -->
    <section class="main-doctor" id="scroll-target">
        <div class="container-fluid">
                 <div class="col-md-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/team-1.jpg" class="img-circle img-responsive center-block" alt="">
                    <h3 class="text-center">Dr. David Riha</h3>
                </div>
                <div class="col-md-1 text-center">
                    <svg class="quote-svg visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/quote.svg" />
                    </svg>
                    <svg class="quote-svg hidden-lg hidden-md" height="30" width="176" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="30" width="176"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/quote-rotate.svg" />
                    </svg>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8">
                    <div>
                         <h4 class="animate-this">„Wir sehen unsere Patienten als Partner und betrachten Sie in ihrer Gesamtheit“</h4>
                        <p>Ich habe am 1. Juli 2015 das Laserzentrum.Dental übernommen. Nach meinem Studium an der Charité Univesitätsmedizin Berlin war ich im Zuge meiner weiteren beruflichen Ausbildung in verschiedenen Praxen und Kliniken tätig. Meine Tätigkeiten umfassten unter anderem auch die Leitung der Kiefergelenk-Sprechstunde am Universitätsklinikum Rostock, wissenschaftliche Mitarbeit in der Polikilinik für zahnärztliche Prothetik und Werkstoffkunde des Universitätsklinikums Rostock sowie chirurgische, prothetische und funktionsdiagnostische Tätigkeit in einem zahnmedizinischen Fachzentrum im Zentrum von Berlin. Durch stetige Weiterbildungen bieten wir Ihnen Zahnmedizin auf dem aktuellen Stand von Wissenschaft und Technik! </p>
                    </div>
                </div>
                <div class="clearfix"></div>
             <div class="row">
                <div class="gradient-hr">
                </div>
            </div>
        </div>
    </section>
    <!-- SECOND DOCTOR -->
    <section class="main-doctor">
        <div class="container-fluid">
                 <div class="col-md-3 col-md-push-9">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/team-2.jpg" class="img-circle img-responsive center-block" alt="">
                    <h3 class="text-center">Dr. Hubert Stieve</h3>
                </div>
                <div class="col-md-1 col-md-push-5 text-center">
                    <svg class="quote-svg horizontal-flip visible-lg visible-md" height="176" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="176" width="30"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/quote.svg" />
                    </svg>
                    <svg class="quote-svg hidden-lg hidden-md" height="30" width="176" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="30" width="176"  xlink:href="<?php echo get_template_directory_uri(); ?>/img/quote-rotate.svg" />
                    </svg>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <div>
                         <h4 class="text-right animate-this">„Die individuellen Wünsche unserer Patienten und die Möglichkeiten der modernen Zahnmedizin im Vordergrund! “</h4>
                         <p class="text-right">Ich habe mein Examen an der Universität Mainz abgelegt. Durch meine anschliessende Tätigkeit im General Hospital Frankfurt wurde ich von der amerikanischen Zahnheilkunde geprägt und halte seither engen beruflichen Kontakt zu Kollegen und Akademien jenseits des Atlantiks. Der Marine bin ich seit dem Wehrdienst als Sanitätsoffizier mit vielfachen Bord-und Auslandseinsätzen sowie Reserveübungen und medizinischen Lehrgängen (z.B. Tauchmedizin) verbunden. Seit der Übernahme des Laserzentrums durch Dr. Riha im Juli 2015 stehe ich neben diesem für Beratung und Behandlung in reduzierter Stundenzahl zur Verfügung. Hier widme ich mich neben den bisherigen Aufgaben besonders den Aufgaben der zahnärztlichen Schlafmedizin. Diese fängt schon bei der zahnärztlichen Untersuchung an und geht weit über die Herstellung einer sog. "Schnarcherschiene" hinaus.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            <div class="row">
                <div class="gradient-hr">

                </div>
            </div>
        </div>
    </section>
    <!-- OTHER DOCTORS -->
     <section class="other-doctors visible-xs visible-sm nopad">
        <div class="container-fluid nopad">
            <div id="slider">
              <button class="control_next btn"><i class="fa fa-angle-right"></i></button>
              <button class="control_prev btn"><i class="fa fa-angle-left"></i></button>
              <ul>
              <?php 
                  query_posts(array( 
                      'post_type' => 'team',
                      'showposts' => -1 
                  ) );  
              ?>
              <?php while (have_posts()) : the_post(); ?>
                <?php 
                  $image = get_field( 'image' )['sizes']['large'];
                  $text = get_field( 'text' );
                  $name = get_field( 'name' );
                 ?>
                <li>
                    <div class="col-md-4 single-doctor">
                       <img src="<?php echo $image ?>" class="img-circle img-responsive" alt="">
                       <h3><?php echo $name ?></h3>
                       <p><?php echo $text ?></p>
                   </div>
                </li>
              <?php endwhile;?>
              <?php wp_reset_query(); ?>
                
                
              </ul>
            </div>
        </div>
    </section>
    <!-- MOBILE -->
    <section class="other-doctors hidden-xs hidden-sm">
        <div class="container-fluid">
            <div>
              <?php 
              $i = 1;
                  query_posts(array( 
                      'post_type' => 'team',
                      'showposts' => -1 
                  ) );  
              ?>
              <?php while (have_posts()) : the_post(); ?>
                <?php 
                  $image = get_field( 'image' )['sizes']['large'];
                  $text = get_field( 'text' );
                  $name = get_field( 'name' );
                 ?>

                    <div class="col-md-4 single-doctor">
                       <img src="<?php echo $image ?>" class="img-circle img-responsive" alt="">
                       <h3><?php echo $name ?></h3>
                       <p><?php echo $text ?></p>
                   </div>
                   <?php if($i % 3 == 0) {echo '<div class="clearfix"></div>';} ?>
                   <?php $i++; ?>

              <?php endwhile;?>
              <?php wp_reset_query(); ?>
                
                
              </div>
            </div>
        </div>
    </section>


<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>


<?php 
get_footer();