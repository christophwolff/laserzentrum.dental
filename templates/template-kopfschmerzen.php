<?php
/*
 * Template Name: Kopfschmerzen
 *
 */

get_header(); ?>

    <section class="container-fluid leistungen">
        <header class=" col-xs-12">
            <h2>Kopfschmerzen, Migräne, Nacken- und Rückenverspannungen sowie Wirbelsäulenprobleme <strong>haben oft etwas mit den Zähnen zu tun</strong></h2>
        </header>
        <div class="col-sm-10">
            <p>Viele Patienten sind zermürbt und es gibt anscheinend keine Hilfe für ihre Beschwerden.. Hier setzt die zahnärztliche Funktionsdiagnostik ein. Dies ist ein seit Jahren wissenschaftlich untersuchtes und anerkanntes Verfahren der Zahnmedizin(Heilkunde...etc).</p>
        </div>
        <div class="clearfix"></div>
        <div class="panel-separation charts">
            <!-- CHART 15 -->
            <div class="chart col-md-12">
                <div class="col-md-3 col-xs-12 chart-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/chart-15.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-9 col-xs-12 nopad">
                    <p>Ca. 15% der Bevölkerung sind von Kopf- und Nackenschmerzen oder Kiefergelenkbeschwerden betroffen.</p>
                </div>
            </div>
            <!-- CHART 10 -->
            <div class="chart col-md-12">
                <div class="col-md-3 col-xs-12 chart-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/chart-10.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-9 col-xs-12 nopad">
                    <p>Ca. 10% der Bevölkerung müsste dringend eine gezielte CMD-Behandlung erhalten, um Schmerzen zu lindern und gravierende Folgeschäden wie einen Bandscheibenvorfall oder irreparable Gelenkschäden zu verhindern.</p>
                </div>
            </div>
            <!-- CHART 60 -->
            <div class="chart col-md-12">

                <div class="col-md-3 col-xs-12 chart-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/chart-60.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-9 col-xs-12 nopad">
                    <p>Etwa 60% der chronischen Schmerzen im Kopf/Nacken/Schulterbereich haben Ihren Ursprung in einer Funktionsstörung des Kausystems. </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-xs-12">
            <h4>Ein kostenloser, schmerzloser  Schnelltest in unserer Praxis gibt Ihnen Auskunft darüber, ob Sie an einer CMD oder Funktionsstörung leiden. </h4>
        </div>
        <div class="col-sm-10 typische">
            <h2>Typische Warnsignale</h2>
        </div>
        <div class="col-sm-12 six-panels">
            <!-- TOP PANEL -->
            <div class="col-md-4">
                <h3>Kopfschmerzen, Migräne & Gesichtsschmerz</h3>
                <p>Ursachen für Kopfschmerzen können überlastete Kaumuskeln sein. Darüber hinaus ist es möglich, dass diese Schmerzen aus der Tiefe ausstrahlen und im gesamten Kopfbereich Schmerzen hervorrufen.</p>
            </div>
            <div class="col-md-4">
                <h3>Schmerzen beim Kauen</h3>
                <p>Durch Knirschen, Pressen oder einen ungleichmäßigen Biss kann die Kaumuskulatur überbelastet sein. Eine Regeneration findet nicht statt und führt zu  starken Beschwerden. Mit einer digitalen Bissanalyse (T-Scan) gehen wir der Ursache auf den Grund!</p>
            </div>
            <div class="col-md-4">
                <h3>Kiefergelenkschmerzen oder -geräusche</h3>
                <p>Kiefergelenksschmerzen, Knack-und Reibegeräusche können verschiedene Ursachen haben. Nur mit einer genauen Diagnostik kann eine Abklärung und geeignete Therapie erfolgen.</p>
            </div>
            <div class="clearfix"></div>
            <!-- BOTTOM PANEL -->
            <div class="col-md-4">
                <h3>Ohrgeräusche & Tinnitus </h3>
                <p>Trotz intensiver Forschung sind die physiologischen Zusammenhänge von CMD & Tinnitus noch nicht eindeutig geklärt. In manchen Fällen gelingt es, mittels einer CMD-Therapie auch den Tinnitus zu verbessern.</p>
            </div>
            <div class="col-md-4">
                <h3>Empfindliche Zahnhälse </h3>
                <p>Knirschen und Pressen können zu freiliegenden Zahnhälsen führen. Diese empfindlichen Areale sind äußeren Einflüssen schutzlos ausgeliefert. Eine Überempfindlichkeit gegenüber Hitze und Kälte ist die Folge.</p>
            </div>
        </div>
        <div class="col-xs-12">
            <h4>Die Behandlung der Cranio-Mandibuläre Dysfunktion ist zur Linderung oder Heilung der Beschwerden und Vermeidung von Folgeschäden absolut wichtig!</h4>
        </div>
        <div class="col-xs-12 panel-separation">
            <h2>Diagnose</h2>
        </div>
        <div class="col-xs-12 col-sm-9">
            <p>Zur Funktionsanalyse gehören eine akribische Untersuchung der Zähne, der Kiefergelenke, der beteiligten Muskulatur sowie verschiedene digitale Aufzeichnungen.</p>
        </div>
    </section>
    <!-- VIDEO -->
    <div class="video video-kopfschmerzen">
        <div class="container-fluid">
            <div id="btn-vid">
                <a class="btn btn-blue" data-toggle="modal" data-target="#myModal">
                    <svg class="video-icon" height="18" width="18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
                        <image x="0" y="0" height="18" width="18" xlink:href="<?php echo get_template_directory_uri(); ?>/img/play.svg" />
                    </svg>
                    Video abspielen
                </a>
             </div>
        </div>
    </div>
     <!-- MODAL -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
         <div class="modal-body">
            <div class="iframe-wraper">
                <iframe  src="https://www.youtube.com/embed/Pk3VboPML_M?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
         </div>
       </div>
     </div>
   </div>
    <section class="container-fluid leistungen-next">
        <div class="col-xs-12 therapie">
            <h2>Therapie</h2>
        </div>
        <div class="col-sm-9 col-xs-12">
            <p>In der Regel wird eine Therapie mit einer Aufbißschiene und spezifischer physiotherapeutischer Behandlung eingeleitet. Weiterführende Therapien beinhalten die Wiederherstellung einer sogenannten frontzahngeführten Okklusion. Diese wird mithilfe eines <strong>T-Scan</strong> im 1/1000stel Sekunden-Abstand bei Kieferschluss behutsam "feinjustiert", wobei fehelnde Zahnkontakte aufgebaut und störende Kontakte um bis zu 50/1000mm reduziert werden. Dieses Verfahren wird nur von einer handvoll entsprechend ausgebildeter Zahnärzte weltweit ausgeübt. Die Wiederherstellung der IACGD (Immediate Anterior Complete Guidance Development) wurde an der University of Maryland, School of Dentistry, Boston, MA unter R. Kerstein entwickelt. </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-10 grey-bg">
            <p>Wir halten zum Thema CMD regelmässig Vorträge in unserer Praxis. In diesem ca. 1-stündigen Vortrag erklären wir Ihnen die Zusammenhänge zwischen Zähnen, Biss und Fehlhaltungen sowie die modernen Behandlungsmethoden, die uns heute zur Verfügung stehen. Sie sind herzlich eingeladen! Sie können sich gern telefonisch vormerken lassen und die Termine unter News&Termine oder unter 04331 3526 930 im Laserzentrum erfragen. </p>
        </div>
    </section>

<?php echo get_template_part( 'templates/template-parts/content', 'appointment' ); ?>

<?php
get_footer();