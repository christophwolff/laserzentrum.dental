<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package laserzentrum.dental
 */

get_header(); ?>

<section class="container-fluid leistungen">
	<div class="container-fluid ">
		<div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
					<header class=" col-xs-12 col-sm-9">
			            	<h2><?php the_title(); ?></h2>
			        </header>
			        <div class="col-sm-9">
			  			<p><?php the_content(); ?></p>
			  		</div>
			<?php endwhile; // end of the loop. ?>		
		</div>
	</div>	
</section>

<?php
get_footer();
