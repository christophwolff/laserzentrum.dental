
<?php $jobs_query = new WP_Query( array( 'category_name' => 'jobs' ) ); ?>

<?php if ($jobs_query->have_posts()): while ($jobs_query->have_posts()) : $jobs_query->the_post(); ?>

<article class="big" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="row news-head">
                <header class="col-sm-9">
                	<h2>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h2>
                </header>
                <div class="col-sm-3 text-right news-date"><?php the_time('F j, Y'); ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-md-12 news-img">
					<!-- post thumbnail -->
					<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail( 'news', array( 'class' => 'img-responsive' ) ); // Declare pixel size you need inside the array ?>
						</a>
					<?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 news-content">
                    <p class=""><?php laserzentrum_excerpt('laserzentrum_excerpt_index') ?></p>
                    <button href="<?php the_permalink(); ?>" class="btn btn-default">Lesen</button>
                </div>
            </div>
        </article>

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, keine Artikel gefunden', 'laserzemtrum_textdomain' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>

<?php wp_reset_query(); ?>
<?php wp_reset_postdata(); ?>